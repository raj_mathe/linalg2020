# Woche 11 #

## SKA 11 ##

### Aufgabe 12 ###

Gegeben sei

    A = -1   1
         2   0
         3   1

A ist in ℝ^{3 x 2}
**Zu finden:** Matrizen P, Q, so dass P·A·Q im Format wie in Satz 6.3.10
Offensichtlich müssen

    P ∈ ℝ^{3 x 3}
    Q ∈ ℝ^{2 x 2}

gelten. Da bei X·Y müssen #col(X), #row(Y) übereinstimmen, weil
wenn man die Matrixmultiplikation ausführt, dann multipliziert man
    - Zeilen aus X
    mit
    - Spalten aus Y.
Im Gaußverfahren

    A —> E1·A —> E2·E1·A —> E3·E2·E1·A ... —> (E_r·E_{r-1}·...·E3·E2·E1)·A

—> Wir wollen (E_r·E_{r-1}·...·E3·E2·E1) als einzige Matrix erfassen, also als P.

Wir führen A in ein augmentiertes System mit der 3x3 Identitätsmatrix auf

    -1  1 | 1 0 0
     2  0 | 0 1 0
     3  1 | 0 0 1

und führen das Gaußverfahren darauf auf. Dann geschieht (effektiv) parallel

    linke  Hälfte: A —> E1·A —> E2·E1·A —> E3·E2·E1·A ... —> (E_r·E_{r-1}·...·E3·E2·E1)·A
    rechte Hälfte: I —> E1·I —> E2·E1·I —> E3·E2·E1·I ... —> (E_r·E_{r-1}·...·E3·E2·E1)·I
                                                            = (E_r·E_{r-1}·...·E3·E2·E1)
                                                            = P

Gaußverfahren:

    -1  1 | 1 0 0
     2  0 | 0 1 0
     3  1 | 0 0 1

    Zeilen 1 und 2 tauschen:

     2  0 | 0 1 0
    -1  1 | 1 0 0
     3  1 | 0 0 1

    Zeile_2 <— 2·Zeile_2 + Zeile_1
    Zeile_3 <— 2·Zeile_3 - 3·Zeile_1

    2   0  | 0   1   0
    0   2  | 2   1   0
    0   2  | 0  -3   2

    Zeile_3 <— Zeile_3 - Zeile_2

    2   0  |  0   1   0
    0   2  |  2   1   0
    0   0  | -2  -4   2

    Zeile_1 <— Zeile_1 / 2
    Zeile_2 <— Zeile_2 / 2

    1   0  |  0   1/2   0
    0   1  |  1   1/2   0
    0   0  | -2    -4   2

Also gilt mit

    P = 0   1   0
        2   1   0
       -2  -4   2

Dass P·A = Form aus Satz 6.3.10.
Setze Q := 2 x 2 Identitätsmatrix
Dann

    P·A·Q = P·A = Matrix im Format aus Satz 6.3.10

### Anderes nicht so glückliches Beispiel ###

Angenommen wir hätten A als 3 x 5 Matrix und nach Ausführung des o. s. Verfahrens

    0 1 0 0 0 |  0   1/2   0
    0 0 0 1 0 |  1   1/2   0
    0 0 0 0 0 | -2    -4   2

erzielt. Dann würden wir P wie oben setzen.
Aber wir müssen noch Q bestimmen.
Das können wir einfach durch Permutationen erreichen:

        0 1 0 0 0   1 0 0 0 0
        1 0 0 0 0   0 0 0 1 0
    Q = 0 0 1 0 0 · 0 0 1 0 0
        0 0 0 1 0   0 1 0 0 0
        0 0 0 0 1   0 0 0 0 1

Oder mit Gaußverfahren, transponieren wir und augmentieren wir mit der 5x5 Identitätsmatrix:

    0   0   0 | 1   0   0   0   0
    1   0   0 | 0   1   0   0   0
    0   0   0 | 0   0   1   0   0
    0   1   0 | 0   0   0   1   0
    0   0   0 | 0   0   0   0   1

    Zeile1 und Zeile2 vertauschen:

    1   0   0 | 0   1   0   0   0
    0   0   0 | 1   0   0   0   0
    0   0   0 | 0   0   1   0   0
    0   1   0 | 0   0   0   1   0
    0   0   0 | 0   0   0   0   1

    Zeile2 und Zeile4 vertauschen:

    1   0   0 | 0   1   0   0   0
    0   1   0 | 0   0   0   1   0
    0   0   0 | 0   0   1   0   0
    0   0   0 | 1   0   0   0   0
    0   0   0 | 0   0   0   0   1

Rechte Hälfte __transponiert__:

        0   0   0   1   0
        1   0   0   0   0
    Q = 0   0   1   0   0
        0   1   0   0   0
        0   0   0   0   1

## Lineare Ausdehnung mit Komplikationen... ##

Betrachte

    u1 = (1, 1, 0, 4)ᵀ
    u2 = (1, 0, 0, 4)ᵀ
    u3 = (0, 1, 0, 0)ᵀ
    u4 = (1, -1, 0, 4)ᵀ

und φ : ℝ^4 —> ℝ^2 partiell definiert

    φ(u1) = (8, 1)ᵀ
    φ(u2) = (4, 5)ᵀ
    φ(u3) = (4, -4)ᵀ
    φ(u4) = (0, 9)ᵀ

Beachte:
    {u1, u2} lin. unabh.
    u3, u4 ∈ Lin{u1, u2}:
        u3 = u1 - u2
        u4 = u2 - u3 = u2 - (u1 - u2) = 2·u2 – u1

Darum müssen

    φ(u3) = φ(u1) - φ(u2)
    φ(u4) = 2·φ(u2) – φ(u1)

gelten.

Wenn nicht erfüllt ==> ex. keine lineare Ausdehnung.
Wenn erfüllt       ==> ex. eine lineare Ausdehnung:

Setze

    u1' = u1
    u2' = u2
    ---> {u1', u2'} lin. unabh.
    ---> {u1', u2'} lässt sich zu einer Basis
         {u1', u2', u3', u4'} von ℝ^4

Wähle v3, v4 ∈ ℝ^2 beliebig und setze

    φ(u1') := (8, 1)ᵀ
    φ(u2') := (4, 5)ᵀ
    φ(u3') := v3
    φ(u4') := v4

Dann laut Satz 6.1.13. ex. eine (eindeutige) lineare Abb.
    φ : ℝ^4 —> ℝ^2
mit

    φ(u1') = (8, 1)ᵀ
    φ(u2') = (4, 5)ᵀ
    φ(u3') = v3
    φ(u4') = v4
