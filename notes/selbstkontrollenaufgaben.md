# Fragen zur Selbstkontrolle #

Hier eine zufällige Stichprobe von Fragen für die Klausurvorbereitung.

## Verschiedene Fragen über Dimension ##

1. Sei V ein Vektorraum und 0 ∈ V der Nullvektor. Was ist dim({0}) ?
2. Sei V ein Vektorraum und u₁, u₂, u₃, u₄ ∈ V. Was sind mögliche Werte von dim(lin{u₁, u₂, u₃, u₄}) ?
3. Sei W ein Vektorraum über einem Körper K und U, V ⊆ W lineare Unterräume.
    - Wie wird der lineare Unterraum U + V definiert?
    - Wie verhalten sich dim(U) und dim(U+V)?
    - Wie verhalten sich dim(V) und dim(U+V)?
    - Wie verhalten sich dim(U+V) und dim(W)?
4. Gib die **Dimensionsformel für Vektorräume** an.
5. Seien W ein Vektorraum und U, V ⊆ W lineare Unterräume.
    Angenommen, dim(W) = 10 und dim(U) = 6 und dim(V) = 8.
    Was sind die möglichen Werte von dim(U ∩ V)?
6. Gib die **Dimensionsformel für lineare Abbildungen** an.
7. ρ : U ⟶ V sei eine injektive lineare Abbildung. Was können wir über dim(U) und dim(V) sagen?
8. Wie wird der Rang einer linearen Abbildung, ψ : U ⟶ V definiert?

## Verschiedene Fragen über lineare Unterräume ##

1. Was sind die Axiome eines linearen Unterraums?
2. Seien U, V Vektorräume über einem Körper, K. Wie wird der Vektorraum U × V definiert?
3. Sei R ⊆ U × V eine Relation. Wie prüft man, ob R ein linearer Unterraum ist?
    (D. h. packe die Aussagen noch genauer aus, was in diesem Kontext zu zeigen wäre.)

## Verschiedene Fragen über Basis ##

1. Gib eine Basis für den Vektorraum alle Polynome ≤ 4. Grades über ℝ an.
2. Gib eine Basis für den Vektorraum alle 3 x 4 Matrizen an. Was ist die Dimension dieses Vektorraums?
3. Was ist die Dimension des Vektorraums aller m x n Matrizen?
4. Wie bestimmt man die Basis des Lösungsraums einer Matrix?
5. Wie bestimmt man die Basis des Spaltenraums einer Matrix?

## Verschiedene Fragen über axiomatische Relationstypen ##

1. Was sind die Axiome einer partiellen Ordnungsrelation?
2. Was muss zusätzlich gelten, damit eine partielle Ordnungsrelation eine lineare Ordnungsrelation (auch »total« genannt) ist?
3. Was sind die Axiome einer Äquivalenzrelation?

## Verschiedene Aspekte von Beweisen ##

In jedem der Aufgaben (ohne sie die Beweise komplett auszuführen), bestimme,
(1) **was _zu zeigen_ ist** und (2) **wie man einen Beweis strukturieren kann**.

### Aufgabe 1. ###

    Sei W ein Vektorraum über einem Körper, K.
    Seien U, V lineare Unterräume von W.
    Zeige, dass U ∩ V ein lineare Unterraum von W ist.

### Aufgabe 2. ###

    Sei ψ : U ⟶ U eine lineare Abbildung, wobei U ein Vektorraum über Körper K ist.
    Sei λ ∈ K.
    Ein Vektor, x, heißt Eigenvektor mit Eigenwert λ, wenn ψ(x) = λx.
    Zeige, dass ρ genau dann einen Eigenvektor mit Eigenwert λ besitzt, wenn dim(Kern(ψ - λ)) > 0.

(_Hier bezeichnet ψ - λ die lineare Abbildung U ⟶ U, x ⟼ ψ(x) - λx._)

# Lösungen #

## Verschiedene Fragen über Dimension ##

1. 0
2. 0, 1, 2, 3, 4
3. .
    - U + V = { u+v | u ∈ U, v ∈ V }
    - U ⊆ U + V, ⟹ dim(U) ≤ dim(U + V)

            {u1, u2, ..., u_n} eine Basis für U
            {v1, v2, ..., v_m} eine Basis für V
            {u1, u2, ..., u_n, v_i1, ..., v_ir} eine Basis für U + V

    - V ⊆ U + V, ⟹ dim(V) ≤ dim(U + V)
    - U + V ⊆ W, ⟹ dim(U + V) ≤ dim(W)

        max{dim(U), dim(V)} ≤ dim(U + V) ≤ dim(W)

4. dim(U + V) = dim(U) + dim(V) – dim(U ∩ V)
5. 4,5,6. Dimensionsformel anwenden:

        dim(U ∩ V) = dim(U) + dim(V) - dim(U + V) = 6 + 8 - dim(U + V)

        max{dim(U), dim(V)} ≤ dim(U + V) ≤ dim(W)
        ⟹ 8 ≤ dim(U + V) ≤ 10
        ⟹ 6 + 8 - 10 ≤ 6 + 8 - dim(U + V) ≤ 6 + 8 - 8
        ⟹ 4 ≤ dim(U ∩ V) ≤ 6

6. für ϕ : U ⟶ V linear gilt dim(U) = dim(Kern(ϕ)) + dim(Bild(ϕ))
7. für ρ : U ⟶ V linear, injektiv dim(U) ≤ dim(V)
8. für ψ : U ⟶ V linear, definiert man Rang(ψ) = dim(Bild(ψ))

## Verschiedene Fragen über lineare Unterräume ##

1. W sei ein Vektorraum über einem Körper K. Sei U ⊆ W eine Teilmenge

    - NL: U ≠ Ø
    - ADD: U unter Addition stabil
    - SKM: U unter Skalarmultiplikation stabil

    oder

    - NL + LK: U unter linearen Kombinationen stabil:

            Seien u1, u2 ∈ U, und seien α1, α2 ∈ K.
            ZU ZEIGEN:  α1·u1 + α2·u2 ∈ U.
            ...
            ...
            Also gilt α1·u1 + α2·u2 ∈ U.

2. U × V:
    - Elemente: (u,v), u ∈ U, v ∈ V
    - Vektoraddition: (u,v) + (u',v') = (u+u', v+v')
    - Skalarmultiplikation: α·(u, v) = (α·u, α·v)

3. Sei R ⊆ U × V. Dann R linearer Untervektorraum ⟺
    - NL: R ≠ Ø
    - LK:

            Seien (u1,v1), (u2,v2) ∈ R, und seien α1, α2 ∈ K.
            ZU ZEIGEN:  α1·(u1,v1) + α2·(u2,v2) ∈ R,
            m. a. W. (α1·u1 + α2·u2, α1·v1 + α2·v2) ∈ R ist zu zeigen.
            ...
            ...
            Also gilt (α1·u1 + α2·u2, α1·v1 + α2·v2) ∈ R.

## Verschiedene Fragen über Basis ##

1. dim(·) = 5, eine Basis ist: 1, x, x^2, x^3, x^4
2. eine Basis bilden bspw. { E_ij : 1≤i≤4, 1≤j≤3 }, also die 12 Matrizen

        ( 1 0 0 0 )   ( 0 1 0 0 )      ( 0 0 0 0 )
        ( 0 0 0 0 ),  ( 0 0 0 0 ), ... ( 0 0 0 0 ).
        ( 0 0 0 0 )   ( 0 0 0 0 )      ( 0 0 0 1 )

3. m·n
4. A sei die Matrix.
    - A —> Zeilenstufenform (am besten normalisiert, aber muss nicht sein)
    - anhand Zeilenstufenform Bestimme freie Unbekannten und schreibe Lösung für unfreie Unbekannten in Bezug auf freie auf.
    - 0-1 Trick (setze alle freie auf 0 und jeweils eine auf 1 (oder ≠ 0) ==> bestimme Lösung)
    - ---> diese bilden eine Basis des Lösungsraums, das heißt von {x | Ax=0}
    - **Zur Kontrolle:** prüfen, dass Ae = 0 für alle e in Basis
    - **Beachte:** dim(Kern(A)) = Größe dieser Basis.
5. A sei die Matrix.
    - A —> Zeilenstufenform
    - merke die Stellen wo Treppen sind ---> entsprechende Spalten in A bilden eine Basis
    - **Beachte:** dim(Bild(A)) = Größe dieser Basis
    - **Zur Kontrolle:** prüfen, dass die Dimensformel für lineare Abbildungen gilt,
    d. h. dim(Kern(A)) + dim(Bild(A)) = dim(Inputvektorraum) = Anzahl der Spalten von A insgesamt

## Verschiedene Fragen über axiomatische Relationstypen ##

Testet selbst, dass ihr die Axiome kennt (oder wisst, wo im Skript sie zu finden sind)
und wie ihr im Beweis mit ihnen umgeht / wie ihr die für eine gegeben konkrete Relation zeigt! 🙂

## Verschiedene Aspekte von Beweisen ##

### Aufgabe 1. ###

    Zu zeigen: (1) U ∩ V ≠ Ø,
    und (2) für u1, u2 ∈ U ∩ V und a, b ∈ K
        gilt a·u1 + b·u2 ∈ U ∩ V

    Zu (1):
        ...
        ...
        ...
        also ist .... in U ∩ V
        also ist U ∩ V nicht leer.

    Zu (2): seien u1, u2 ∈ U ∩ V und a, b ∈ K.
    Dann
        ...
        ...
        ...
        a·u1 + b·u2 ∈ U ∩ V.

### Aufgabe 2. ###

    (⟹) Sei angenommen, .... [1. Aussage]. Zu zeigen: .... [2. Aussage].
    ...
    ...
    ...
    Also gilt [2. Aussage].

    (⟹) Sei angenommen, .... [2. Aussage]. Zu zeigen: .... [1. Aussage].
    ...
    ...
    ...
    Also gilt [1. Aussage].
