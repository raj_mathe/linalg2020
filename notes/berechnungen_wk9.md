# Woche 9 #

(Für die Berechnungen haben wir Octave benutzt.)
## Aufgabe ähnlich wie ÜB9-1 ##

U = lin{u1, u2}
V = lin{v1, v2, v3}

### U ⊆ V ? ###
#### Beispiel 1 ####

    u1 = (1 1 0 0)ᵀ
    u2 = (-1 1 0 0)ᵀ

    v1 = (4 0 0 0)ᵀ
    v2 = (1 4 0 0)ᵀ
    v3 = (1 0 1 0)ᵀ

    Anmerkung: lin{u1, u2} ⊆ lin{v1, v2, v3} <===> u1, u2 ∈ lin{v1, v2, v3}

    Setze A := (v1 v2 v3 u1 u2)
    ---> auf Zeilenstufenform reduzieren
    ---> zeigen, dass in homogenen LGS Ax = 0 x4 und x5 freie Unbekannte sind.
    ---> ja
    ---> lin{u1, u2} ⊆ lin{v1, v2, v3}

#### Beispiel 2 ####

    u1 = (1 1 0 1)ᵀ
    u2 = (-1 1 0 0)ᵀ

    v1 = (4 0 0 0)ᵀ
    v2 = (1 4 0 0)ᵀ
    v3 = (1 0 1 0)ᵀ

    Setze A := (v1 v2 v3 u1 u2)
    ---> auf Zeilenstufenform reduzieren
    ---> zeigen, dass in homogenen LGS Ax = 0 x4 und x5 freie Unbekannte sind.
    ---> nein
    ---> also lin{u1, u2} ⊈ lin{v1, v2, v3}

### Basis von V/U ###

    --> Beispiel 1.

    u1 = (1 1 0 0)ᵀ
    u2 = (-1 1 0 0)ᵀ

    v1 = (4 0 0 0)ᵀ
    v2 = (1 0 1 0)ᵀ
    v3 = (1 4 0 0)ᵀ

    Schreibweise für Äquivalenzklassen:
        [v] = v + U
    --> die Elemente in V/U

    Setze A := (u1 u2 v1 v2 v3)
    ---> auf Zeilenstufenform reduzieren
    ---> bestimmen, welche Variablen frei / unfrei sind
        ---> bestimme die Basis durch die Spalten, die den unfreien Variablen entsprechen
    --->
        x3, x5 sind frei
        x1, x2, x4 nicht frei
    ---> v2 + U (entspricht x4) bildet eine Basis


## SKA 9-5 ##

    Basis für U:
    u1 = (1 1 0)ᵀ
    u2 = (0 1 1)ᵀ
    Basis für V = ℝ^3:
    v1 = (1 0 0)ᵀ
    v2 = (0 1 0)ᵀ
    v3 = (0 0 1)ᵀ

    A = (u1, u2, v1, v2, v3)
    ---> Zeilenstufenform: x1, x2, x3 nicht frei; x4, x5 frei
    ---> V / U = lin {v1 + U} = lin { e1 + U }
        und dim(V/U) = 1

        Beachte: v2 = u1 - v1      ===> v2 + U = -(v1 + U)
                v3 = (u2-u1) + v1 ===> v3 + U = v1 + U



## UB9-2 (Bsp) ##

    Seien

    v1 = (1 0 0 4 1)ᵀ
    v2 = (0 1 0 8 0)ᵀ
    v3 = (-3 0 0 0 1)ᵀ

    φ : ℝ^3 ---> ℝ^5
    sei linear mit
        φ(e_i) = v_i für alle i

    1. Sei x = (x1, x2, x3) ∈ ℝ^3
        φ(x1,x2,x3)
        = φ(x)
        = φ(x1·e1 + x2·e2 + x3·e3)
        = φ(x1·e1) + φ(x2·e2) + φ(x3·e3)
        = x1·φ(e1) + x2·φ(e2) + x3·φ(e3)
        = x1·v1 + x2·v2 + x3·v3
        = Ax
    wobei A = (v1 v2 v3)
        =   1   0  -3
            0   1   0
            0   0   0
            4   8   0
            1   0   1
    Also ist φ = φ_A (siehe Korollar 6.3.15 aus [Sinn2020]).
    Wℝ berechnen den Rang von A, um die Injektivität/Surjektivität/Bijektivität
    von φ zu klassifizieren:
    ---> A in Zeilenstufenform:
            1    0   -3
            0    1    0
            0    0    0
            0    0   12
            0    0    4
        Rang(A) = 3
    ---> A ist eine mxn Matrix mit m=5, n=3
            Rang(A) = 3 ≥ 3 = n ===> φ = φ_A ist injektiv
            Rang(A) = 3 < 5 = m ===> φ = φ_A ist nicht surjektiv
            m ≠ n ===> φ = φ_A ist nicht bijektiv



## UB9-3 (wie man ansetzen kann...) ##

**Zz:** ψ◦ϕ injektiv <===> ϕ injektiv + Kern(ψ) ∩ Bild(ϕ) = {0}

(==>) Angenommen, ψ◦ϕ injektiv.
**Zz:** ϕ injektiv + Kern(ψ) ∩ Bild(ϕ) = {0}.

...

(<==) Angenommen, ϕ injektiv + Kern(ψ) ∩ Bild(ϕ) = {0}.
**Zz:** ψ◦ϕ injektiv

Laut Korollar 6.3.15 aus [Sinn2020] reicht es aus zu zeigen, dass Kern(ψ◦ϕ) = {0}.

Sei x ∈ U beliebig.

**Zz:** x ∈ Kern(ψ◦ϕ) <===> x = 0

        x ∈ Kern(ψ◦ϕ)
        <===> (ψ◦ϕ)(x) = 0
        ..
        .. <--- ϕ injektiv + Kern(ψ) ∩ Bild(ϕ) = {0} ausnutzen !
        ..
        <===> x = 0
