1. **Zz**: Die additive Struktur ist eine kommutative Gruppe.
Wir gehen die Axiome durch:

    - **Zz:** Assoziativität

        ..

    - **Zz:** Kommutativität:

        ..

    - **Zz:** additives Neutralelement (Nullelement)

        ..

    - **Zz:** Existenz additiver Inverser.

        ..
2. **Zz**: Skalarmultiplikation ist assoziativ.
    - ..
3. **Zz**: Skalarmultiplikation ist distributiv.
    - ..
4. **Zz**: $1\cdot u=u$ für alle Elemente $u$
    - ..
