# Repository für Lineare Algebra / Übungsgruppe #

In diesem Repository werden Ressourcen hochgeladen,
zum Beispiel Skripte oder Dokumente für mathematische Argumente.
Gründe hierfür:

- um den Umstand zu vermeiden, per Email, Moodle, BBB, usw. Dateien zu schicken.
- technische Kritzelei irgendwo festzuhalten.

Dieses Repo enthält

1. Code/Codeschnippsel: siehe [code](./code).
2. Dokument inklusive meiner Lösungen zu den Übungsblättern (die nach dem Abgabetermin hochgeladen werden): siehe [docs](./docs) und [docs/loesungen.pdf](./docs/loesungen.pdf).
3. Notizen/Kritzelei für mathematische Argumente, Berechnungen, usw.: siehe [notes](./notes).
4. Protokolle von den Übungsgruppen: siehe [protocol](./protocol).

Im Rest dieses Dokuments werden _Einzelheiten zum Kurs_, das Thema _mathematisches Denken_, und Software-Tipps diskutiert.

## §1. Einzelheiten zum Kurs ##

Die URL zum Kurs findet man hier: <http://www.math.uni-leipzig.de/~sinn/lehre/LA1.html>.

### Wöchentliches Protokoll ###

Jede Woche wird ein Protokoll in Markdown-Dateien im Ordner [/protocol](./protocol) festgehalten.
Beachte, dass wir uns in Wochen 12 + 13 der Wiederholung vor der Klausur widmeten.

### Übungsgruppen ###

Die Übungsgruppen sind Pflichtveranstaltungen.

Jede Woche besteht der Ablauf grob aus folgenden Teilen:

- allgemeine Ankündigungen
- Präsentation von SKA von jeder Gruppe (sofern anstehend)
- Besprechung von ÜB aus vorheriger Woche (sofern korrigiert)
- Besprechung vom Stoff aus VL
- Quiz 10min
- Breakout-Rooms für SKA

Je nach Zeit und Nachfrage fallen manche Dinge aus, damit wir uns den wichtigeren Teilen widmen können.

### Leistungen ###

Klausurzulassung, wenn

- ~~?/? Quizzes~~ ⟶ keine Voraussetzung mehr. Die Quizzes sind freiwillig!
- ≥ 50% der Punkte von 11 Übungsblättern (82,5 Punkte).
- **ACHTUNG:** Es gibt zwar ~~ggf. ein 12. ÜB und~~ mehr Punkte auf einigen Blättern, aber der Schwellwert bleibt bei 82,5 Pkt, so der Professor.

### Klausur ###

Siehe bitte zuerst das **Hinweise**-Blatt auf dem Kurs-Moodle im Beitrag über die Klausurvorbereitung.
Dies enthält Hinweise über relevante Inhalte sowie nützliche Aufgaben zur Wiederholung.

**ANMERKUNG:** Folgende Punkte sind _nur_ als weitere Hinweise zu verstehen.
Generell für eine Klausur muss man den ganzen in der VL behandelten Stoff gemeistert haben.
Folgendes listet nur minimalistisch Aspekte auf,
die auf jeden Fall für die Klausur wichtig sind.
Sie sind nicht unbedingt vollständig.

- Kapitel 1: Müsst ihr allgemein kennen.
  - Konkrete Dinge wie Strecken, Ebenen, usw. kommen eher in Geometrie und hier nicht vor.
  - Eliminationsverfahren und Lösbarkeit von LGS:
    - Lösung des homogenen Systems Ax = 0
    - Lösung des inhomogenen Systems Ax = b
    - Die Äquivalenzen in §6.4 fassen einiges hier viel kürzer zusammen.
- Kapitel 2: Müsst ihr generell können. Insbesondere Umgang mit
  - Axiomen von (partiellen/totalen) Ordnungsrelationen
  - Axiomen von Äquivalenzrelationen
  - Für Funktionen, ƒ : X ⟶ Y
    - der Graph von ƒ
    - Umkehrabbild ƒ¯¹ (wenn ƒ bijektiv ist).
    - ƒ¯¹(B) für B ⊆ Y, d. h. Urbildmengen von B unter ƒ
            (!! und dass dies NICHT dasselbe wie das Inverse ƒ¯¹ : Y ⟶ X ist !!)
    - ƒ(A) für A ⊆ X, d. h. Bild von A unter ƒ
    - Konzepte von Injektivität/Surjektivität/Bijektivität
            (!! und dass sich diese NICHT gegenseitig ausschließen!!)
    - Komposition von Funktionen
- Kapitel 3: Müsst ihr generell können. Insbesondere Umgang mit
  - ℤ/_n_ für konkrete Werte von _n_,
  - Berechnung von Addition, Multiplikation, und Inversen modulo _n_ (for konkrete Werte).
- Kapitel 4:
  - Grundkonzepte wie Inverses in Gruppentheorie.
  - Grundkonzepte von Körpern und Ringen.
  - Umgang mit LGS über Vektorräume über einem Körper K (konkreter Umgang mit 𝔽ₚ für _p_ prim):
    - genau wie bei ℝ-wertigen Matrizen nur mit Addition/Multiplikation/Inversen in 𝔽ₚ (d. h. ℤ/_p_).
    - man muss insbesondere Division durch 0 mod p vermeiden und es hilft,
        die Werte immer als Werte aus {0, 1, 2, ..., p–1} darzustellen.
        Z. B. in 𝔽₅ sollte man eine Matrix

                A = ( 12  -1    4   1 )
                    (  0  80  -17  28 )

            eher darstellen als

                A = ( 2  4  4  1 )
                    ( 0  0  3  3 ),

        damit man vor allem den Rang und die Zeilenstufen richtig erkennt.
- Kapitel 5: Hier müsst ihr generell alles meistern.
  - dass man aus Vektorräumen andere Vektorräume konstruieren kann (z. B. durch Produkte)
  - Unterräume
  - Lin(A) für A ⊆ V, V ein Vektorraum
  - Basis:
    - lineare Unabhängigkeit, Erzeugendensysteme, Basis.
      - Überprüfung von linearer Unabhängigkeit
      - Berechnung von Basen:
        - anhand einer Menge von Vektorren:
          - Reduktion einer Menge von Vektoren auf eine (maximale) linear unabhängige Teilmenge
          - Erweiterung von Vektoren auf eine Basis
        - Basis des Spaltenraums, Bild(A), für eine Matrix, A:
          - A ⟶ Zeilenstufenform ⟶ merke Spaltenpositionen j1, j2, ... wo Stufen sind
          - Die Spalten j1, j2, ... von der Originalmatrix, A, bilden eine Basis von Bild(A)
      - Konkrete Fälle:
        - „kanonische Basis“ von ℝⁿ für beliebige konkrete Werte von _n_.
        - „exotische“ Beispiele von Vektorräume wie der Raum der Polynomen, ℝ\[x\], und deren „kanonische“ Basen.
  - Dimension, Dimensionsformel.
  - Rang:
    - Zeilenrang := #Stufen von A in Zeilenstufenform
    - Spaltenrang := dim(Bild(A))
    - **Lemma:** Zeilenrang = Spaltenrang
    - **Definition:** Rang := Zeilenrang = Spaltenrang;
- Kapitel 6 | 6.1–6.4:
  - lin. Abb
  - Kern, Bild
  - Injektivität, Surjektivität, Bijektivität („Isomorphismus“)
  - Zusammenhang zw. Dimension von U, V und Eigenschaften von einer linearen Abbildung φ : U ⟶ V.
  - !! Lineare Ausdehnung !!
    - insbes. 6.1.13, nur bekommt ihr eine Situation,
        wo ihr eine Definition auf einer Basis + einem zusätzlichen linear abhängigen Element bekommt
  - Das Konzept von Darstellungsmatrizen (aber nicht die Berechnung von Basiswechseln)
  - Invertierbare Matrizen (musst algebraisch/symbolisch damit umgehen können, aber ihr müsst keine Inversen explizit ausrechnen). Ihr müsst aber Konzepte anwenden wie (AB)¯¹ = B¯¹A¯¹,
    und dass der Raum der invertierbaren Matrizen unter Multiplikation stabil sind.
- Kapitel 6 | 6.5+: nicht behandelt.
- Kapitel 7: nicht behandelt

## §2. Mathematisches Denken ##

Mathematik ist eine präzise aber abstrakte Kunst.
Pflegen muss man den Umgang mit zwei Aspekten:

- Anschauung,
- Formalismen.

Es gibt ein Zwischenspiel zwischen beiden dieser Aspekte.

### Anschauung ###

Stichwörter: **Konzepte** (en: _notion_), **Vorstellung**, **Visualisierung**, **Intuition**, ...

Mit _Anschauung_ meinen wir nicht bloß _Visualisierung_,
sondern vielmehr das intuitive Begreifen von mathematischen Konzepten.
Mit Intuition nun ist _nicht_ »common sense« gemeint,
sondern eine Fähigkeit, die man antrainieren muss,
um abstrakte Sachverhalte zu visualisieren und internalisieren,
und um sich mit den mathematischen »Gegenständen« vertraut zu machen.

### Formalismen ###

Stichwörter: **Symbole**, **Notation**, **Axiome**, **Rahmen**, **Aussagen**, **Beweise**, **Argumentation**, ...

Kurz gesagt, die _formalen_ Aspekte bestehen aus technischen Symbolen,
mithilfe derer wir Aussagen schreiben, und der Struktur von Argumenten.

Der Begriff _Formalismus_ geht eigentlich auf die Grundlagen der Mathematik ab der Mitte des 19. Jh zurück.
Ab dieser Zeit fingen Mathematiker an, nicht mehr lose zu berechnen, sondern Erkenntnisse in _formalen Systemen_ aufzuschreiben.
Im Grunde (und im Falle von Church, Turing, Kleene, usw.) legten sie die Bausteine für das moderne Konzept von Berechenbarkeit, Algorithmen, und Rechnern.
Es stellt sich heraus (siehe insbesondere das Löwenheim-Skolem-Tarski Paradoxon),
dass mathematische Aussagen komplett unabhängig von Anschauungen ausgelegt und bewiesen werden können.
Mit anderen Worten, man kann einen »seelenlosen« Rechner mit mathematischen Aufgaben beauftragen,
und dieser wäre ohne jegliche Vorstellungskraft in der Lage,
_richtige_ Berechnungen durchzuführen und Schlüsse zu ziehen.

### Die Rolle von beiden Aspekten ###

Einerseits sind formale Mitteln notwendig,
um Aussagen _klar und eindeutig_ zu formulieren,
und notwendig und hinreichend, um diese zu beweisen.
Andererseits benötigen wir als _denkende Menschen_ aber auch Anschauungen,

1. um formale Aussagen _deuten_ zu können und deren Informationsgehalt zu _begreifen_,
2. damit einem Ideen und Ansätze einfallen, um Behauptungen zu beweisen.

Wir brauchen also die formale Seite, um **präzis** zu kommunizieren und richtig zu argumentieren,
und die anschauliche Seite, um uns überhaupt orientieren zu können.

### Wie trainiere ich das? ###

Es gibt einige Möglichkeiten für verschiedene Lerntypen:

- Selbstlernen: sich alleine mit dem Skript auseinandersetzen. Am besten ein paar Stunden in einem ruhigen Ort wie einem Café, der Bibliothek, zu Hause.
Gründlich die Definitionen und Resultate durchgehen.
- Durch Gruppenarbeit.
- Austausch von konkreten Fragen in eurer Chat-Gruppe oder in online Foren wie stackexchange, math.hashcode, usw..
- In der Übungsgruppe. Bei wichtigen Fragen, die wir gemeinsam bearbeiten, werde ich versuchen, diese in dem Repository festzuhalten.

## §3. Software für Text/Notizen ##

Unter Mathematikern, Wissenschaftlern, (womöglich auch Ingenieuren), und Informatikern sind folgende Optionen sehr beliebt:

- Markdown (+ rendering software od. Extensions auf Editoren wie VSCode)
- sehr einfachen "plain" Texteditoren
- LaTeX
- Zeichenapps wie GoodNotes
- die guten alten Stift + Zettel/Heft ... Vorteil: man muss nicht aufladen ; )

Es lohnt sich, einen guten Allzweck-Editor wie [VSCode](https://code.visualstudio.com/download)
zu installieren.
Mit so etwas kann man etliche gratis Extensions finden,
die bspw. Syntax-Highlighting, Rendering, Code-Schnippsel für LaTeX, md, usw. bieten.

## Software zwecks Berechnungen und Anschauungen ##

Es gibt einige Hilfsmittel, derer man sich bedienen kann, um entweder Konzepte zu visualisieren oder zu berechnen.
**Diese Möglichkeiten sind keineswegs verpflichtend!**

### Python ###

[Hier](https://www.python.org/downloads) kann man den Compiler herunterladen.
Man kann auch ohne Installation python Skripte ausführen:
einfach nach »python compiler online« googeln (oder z. B. <https://repl.it> -> `<>Start coding` —> Sprache auswählen).

**Vorteile:** Da man kein Memory-Allocation o. Ä., oder Typisierung pflegen muss, kann man mit grundlegenden Programmierkenntnissen sehr leicht in Python einsteigen.
Es gibt eine immense Community für Python und man kann sehr schnell online durch Foren u. Ä. Hilfe holen.
Die Python-Dokumentation ist sehr ausführlich und alles ist gut versioniert.
Möglicherweise werden einige von euch etwas im Bereich Data Science machen.
Dafür ist python (aktuell) mit das gängigste Tool.
Generell (nicht nur wegen DS) lohnt es sich, Python (samt Modulen wie **numpy**/**numpy.linalg**, **pandas**, usw.) zu können.

**Nachteile:** Python ist nicht sonderlich schnell, aber bzgl. Geschwindigkeit definitiv besser als Geogebra.
Für Programmierer gibts an Python viel Grund zu meckern (z. B. keine echten privat/public/protected access modifiers, unsauberer Umgang mit Typing.).

Für Visualisierungen von Vektoren wäre Python nicht die beste Option.
Für unseren Kurs würde ich dies nur für die Ausführung von Algorithmen Empfehlen.

**Anmerkung:** Um mit komplexen Zahlen in Python umzugehen, muss man `j` statt `i` für die imaginäre Zahl verwenden.
Diese muss stets mit einem Koeffizienten versehen werden,
d. h.

```py
x = 5 + 8j;
x = 5 + 8*1j;
x = 5 + 1j;
x = 5 - 1j;
x = 5 + 0j;
x = 5;
```

usw. sind erlaubt, aber

```py
x = 5 + 8*j;
x = 5 + j;
x = 5 - j;
```

usw. nicht, weil hier logischerweise `j` als Variable interpretiert wird.

So kann man mit Matrizen umgehen (am besten hierfür ein Skript anlegen und dieses über eine Terminal ausführen):

```py
import math
import numpy as np
import numpy.linalg as la

A = np.asarray([
    [1, 2, 3, 7],
    [0.5, 0, 0, 2 + 1j]
    [0, 0, 0, math.sqrt(2)]
])

x = np.asarray([10, 100, 0, -1])

print(A)
print(A.T)
print(A @ x) # Matrixmultiplikation
print(la.det(A)) # Determinante

B = A @ A.T # eine quadratische (sogar positive!) Matrix
values, vectors = la.eig(B)

print(values) # da B "positiv" ist, sind diese Werte alle reellwertig und ≥ 0
print(vector) # eine Matrix, deren Spalten Eigenvektoren von B sind (entsprechend den Eigenwerten)

Z = np.zeros((4, 2)) # eine 4 x 2 Matrix

print(Z)
```

Es gibt _etliches_, was man mit den `numpy` (und `scipy`) Bibliothek und Vektoren/Matrizen/Tensoren machen kann.
Lass dir niemanden sagen, dass man "besser mit MatLab" (oder `R`) mit Matrizen umgehen kann.
Da _gerade_ diese Sprache für KI optimiert wird, kann man sicher davon ausgehen,
dass die zugrunde liegenden Bibliotheken mit `C` o. Ä. geschrieben und schnell sind.

Natürlich kann und m. E. sollte man kompilierende Sprachen lernen wie `C++` od. `golang`/`Rustlang`/`vlang`, usw.
Aber für Prototypen von komplexen Problemen, mit vielen Möglichkeiten für sauberes Programmieren
bleibt Python unschlagbar.

### Geogebra ###

Diese App ist ein lustiges aber sehr nützliches Programm, um schnell im 2-d Raum ($\mathbb{R}^{2}$) oder 3-d Raum ($\mathbb{R}^{3}$) geometrische Objekte und Konzepte zu realisieren.
Man kann GeoGebra [hier](https://www.geogebra.org/download?lang=de) herunterladen.

**Vorteile:** man braucht hier _null_ Programmierkenntnisse. Mit der App kann man ohne Weiteres direkt loslegen.

**Nachteile:** Man sollte es aber nicht zu weit betreiben, denn diese App wird schnell überfordert.
Es scheint, dass man nicht mehr Dateien lokal speichern kann (?!).
Anscheinend wollen die „klugen“ Betreiber dieser App einen rein online Gebrauch erzwingen 🤦.

### Octave / MatLab ###

**MatLab** (Matrix Laboratory) ist eine in dem Ingenieurwesen bekannte Programmiersprache
zum einfachen Umgang mit Matrizen und allgemein diskreten Methoden.
GNU **Octave** ist lediglich die gratis Variante davon und kann [hier](https://www.gnu.org/software/octave) gefunden werden.
Ich kann dies auf jeden Fall empfehlen, um intuitiv und schnell mit Matrixberechnungen (v. a. mit komplexen Einträgen) umzugehen.
Hier ein paar Beispiele in der Sprache:

Eingabe von Matrizen und Vektoren:

```bash
octave:1> A = [1 4; -7.1  3 + i; 0 8];
octave:2> disp(A);
   1     4
  -7.1   3 + 1i
   0     8
octave:3> A = [1 4; -7.1  3 + i; 0 8].';
octave:4> disp(A);
   1  -7.1      0
   4   3 + 1i   8
octave:5> x = [1 40 3].';
octave:6> disp(x);
    1
    40
    3
```

Zeilenoperationen:

```bash
octave:5> disp(A(2,:));
   4   3 + 1i   8
octave:6> A(2,:) = A(2,:) - 4*A(1,:);
octave:7> disp(A);
    1   -7.1          0
    0   31.4 +  1i    8
```

Matrixmultiplikation:

```bash
octave:1> A = [1 4 8; 3 6 -9]; x = [1 1 1].';
octave:2> b = A*x;
octave:3> disp(b);
    13
    0
octave:4> x_ = A \ b; % äquivalent zu „finde (irgend)eine Lösung zu Ax=b“
octave:5> disp(x_);
   0.41474
   1.22992
   0.95820
octave:6> disp(A*x_); % wird bis auf machine-Fehler gleich b sein
   1.3000e+01
   1.5543e-15
```

**Nachteile:**
Octave/MatLab fördern einen äußerst schlechten Programmierstil.
Gut für kurze Berechnungen aber bis heute gibt es Unternehmen,
die diese Sprache/n für größere Projekte benutzen, was einfach fatal ist.
Octave/MatLab bieten keinen sauberen Umgang mit Typen od. Modulen an.
MatLab ist außerdem sehr teuer und **nicht open source**.

### R ###

Für **R** braucht man

- Den **R** Compiler (siehe z. B. [hier](https://cran.rstudio.com))
- (optional) einen Editor wie **RStudio** (siehe [hier](https://rstudio.com/products/rstudio/download/#download)).

Man kann auch ohne Installation R-Skripte ausführen: einfach nach »R compiler online« googeln (oder z. B. <https://repl.it> -> `<>Start coding` —> Sprache auswählen).

**Vorteile:** man braucht hier nur _sehr minimale_ Programmierkenntnisse.
Diese Sprache wurde für Naturwissenschaftler und Statistiker entwickelt, und Menschen rund um den Globus entwickeln immer neue Packages für alles Mögliche in dieser Sprache.
Es gibt eine große Community und damit kann man für alle Probleme Hilfe finden.
Visualisierung mag zwar umständlicher als mit Geogebra sein, aber ist nicht so schwer.

**Nachteile:**
Man sollte im Laufe seines Studiums **R** nicht _ausschließlich_ bedienen,
denn diese Sprache fördert einen richtig schlechten Programmierstil.
Für die Logiker und Informatiker unter euch, wird es bspw. nerven, dass in **R**-Arrays (sog. lists/vectors) Indexes mit `1` anfangen.
Für Programmierer, wird stören, dass **R** keine saubere Implementierung von Klassen, (lokalen) Imports, usw. anbietet
(diese Dinge existieren, aber sind sehr umständlich).

### Julia ###

Die **Julia**-Programmiersprache soll auch sehr gut sein und kann auf <https://julialang.org> gefunden werden.
Es bedarf hier zumindest Grundkenntnisse von Programmiersprachen.
**Julia** hat den Vorteil von Geschwindigkeit (für kleine Matrixberechnungen irrelevant, aber sobald man mit größeren Datensets umgeht wird dies wichtig sein).
Hierfür muss man sich den Umgang mit Typen aneignen, was generell zu saubererem Code führt.
