# Woche 12 (KW 5, 1.—7.2.) #

## Ablauf ##

- (√) Organisatorische Fragen
    - (√) Übungsblätter / Punkte / Zulassungsbeschränkungen
        - 50% von 11 Blättern (= 82,5)
        - Warten noch Leute auf deren Noten?
    - (√) Klausurvorbereitung
        - Moodle
- (√) Fragen/Feedback zu ÜB11
    - Allgemein: Argumentiere immer mit klaren logischen Zusammenhängen
    „zwischen den Zeilen“ in einem Argument. D. h. ⟹, ⟺, usw. anwenden
    (und immer begründen, wenn nicht trivial).
    - 11·1(b) Beachte die Änderung in der Reihenfolge!
    - warum Rang(A)=n ⟺ Rang(A) ≥ n in Aufgabe 11·2(a):
        - weil Rang(A) = Spaltenrang ≤ n stets gilt!
    - warum Rang(A)=m ⟺ Rang(A) ≥ m in Aufgabe 11·2(b):
        - weil Rang(A) = Zeilenrang ≤ m stets gilt!
- (√) Fragen zum Stoff oder Aufgaben
    - Berechnung von Inversen / Gaußalgorithmus im Falle von endlichen Körpern
        (z. B. 𝔽₅, modulo _p_ für eine Primzahl)
    - lineare Ausdehnung

Berechnungen ---> siehe [/notes/berechnungen_wk12.md](../notes/berechnungen_wk12.md).
(Siehe auch Berechnungen in [Woche 10](../notes/berechnungen_wk10.md).)
