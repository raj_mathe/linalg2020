https://stackedit.io/editor

# SKA 7 #

## SKA 7-1 ##

Vgl. [Skript, **Bsp. 5.1.1, (1)**].

**Behauptung.** Seien $n\in\mathbb{N}$ und $K$ ein Körper.
Dann bildet $K^{n}$, versehen mit _punktweise Addition_
und vermöge $\alpha\cdot(x_{i})_{i=1}^{n}=(\alpha x_{i})_{i=1}^{n}$
definierte Skalarmultiplikation
einen Vektorraum.,

**Beweis.**

1. **Zz**: $(K^{n},+)$ mit punktweise Addition ist eine kommutative Gruppe.

    **Ansatz I.** $(K^{n},+,\mathbf{0})$ ist lediglich die Produktgruppe aus $n$ Kopien von $(K,+,0_{K})$,
also sofort eine kommutative Gruppe.

    **Ansatz II.** Wir gehen die Axiome durch:

    - **Zz:** $(K^{n},+)$ ist assoziativ:

        ..

    - **Zz:** $(K^{n},+)$ ist kommutativ:

        Seien $\mathbf{u}=(u_{i})_{i=1}^{n},\mathbf{v}=(v_{i})_{i=1}^{n}\in K^{n}$.
        Zu zeigen ist, dass
        $(u_{i})_{i}+(v_{i})_{i}=(v_{i})_{i}+(u_{i})_{i}$.

        $$(u_{i})_{i}+(v_{i})_{i}
        =^{\text{Defn}} (u_{i}+v_{i})_{i}
        =^{\ast} (v_{i}+u_{i})_{i}
        =^{\text{Defn}} (v_{i})_{i}+(u_{i})_{i}
        $$

        Die Gleichung in ($\ast$) gilt, weil $(K,+)$ kommutativ ist.

    - **Zz:** $(K^{n},+)$ hat ein Neutralelement

        ..

    - **Zz:** $(K^{n},+)$ hat Inverse

        ..
2. **Zz**: Skalarmultiplikation ist assoziativ.
    - ..
3. **Zz**: Skalarmultiplikation ist distributiv.
Seien $\alpha,\beta\in K$ und $\mathbf{u}=(u_{i})_{i}\in K^{n}$.
Zu zeigen:

    $\begin{array}{rcl}
        \alpha\cdot(\beta\cdot\mathbf{u}) &= &(\alpha\cdot\beta)\cdot\mathbf{u}\\
    \end{array}$

    Es gilt

    $\begin{array}{rcl}
        \alpha\cdot(\beta\cdot\mathbf{u})
            &= &\alpha\cdot(\beta\cdot(u_{i})_{i})\\
            &= &\alpha\cdot(\beta\cdot u_{i})_{i}\\
            &= &(\alpha\cdot(\beta\cdot u_{i}))_{i}\\
            &=^{\ast} &((\alpha\cdot\beta)\cdot u_{i}))_{i}\\
            &= &(\alpha\cdot\beta)\cdot (u_{i}))_{i}\\
            &= &(\alpha\cdot\beta)\cdot \mathbf{u}\\
    \end{array}$

    Gleichung ($\ast$) gilt weil $(K,\cdot)$ assoziativ ist.

4. **Zz**: $1\cdot\mathbf{u}=\mathbf{u}$ für alle $\mathbf{u}\in K^{n}$
    - ..

Also ist $K^{n}$ ein Vektorraum.
**QED**

## SKA 7-2 ##

Einem jeden Element

$$\left(\begin{matrix}
    x_{1,1} &x_{1,2} &x_{1,3}\\
    x_{2,1} &x_{2,2} &x_{2,3}
\end{matrix}\right)$$

aus $M_{2\times 3}(\mathbb{R})$ können wir das Element

$$\left(\begin{matrix}
    x_{1,1}\\
    x_{1,2}\\
    x_{1,3}\\
    x_{2,1}\\
    x_{2,2}\\
    x_{2,3}\\
\end{matrix}\right)$$

zuordnen. Dies ist eine bijektive, lineare Abbildung
$M_{2\times 3}(\mathbb{R})\to\mathbb{R}^{6}$
und (am wichtigsten!) preserviert Addition und Skalarmultiplikation.

## SKA 7-3 ##

Vgl. [Skript, **Bsp. 5.1.1, (4)**].

**Ansatz I:** Direkt.

**Ansatz II:** Ist äquivalent (»isomorphisch«) zu $\mathbb{R}^{3}$.
Aber allgemein (für nicht endliche Mengen, $X$) muss man die Axiome durchgehen.

$f\in\mathop{Abb}(\{a,b,c\},\mathbb{R}) \mapsto v_{f}:=\left(\begin{matrix}f(a)\\f(b)\\f(c)\end{matrix}\right)\in\mathbb{R}^{3}$

Man muss extra zeigen: $v_{f+g}=v_{f}+v_{g}$ und $v_{\alpha\cdot f}=\alpha\cdot v_{f}$ für alle $f,g\in\mathop{Abb}(\{a,b,c\},\mathbb{R})$ und $\alpha\in\mathbb{R}$.

## SKA 7-4 ##

Vgl. [Skript, **Bsp. 5.1.1, (2)+(4)**].

Hinweis: Ein Tuple, $a$, mit Werten in $K$ und Indizes über $\mathbb{N}$
ist eine Kurzhand für eine Funktion ${a:\mathbb{N}\to K}$.
Das gilt eigentlich für alle (unendlichen) Mengen.
(Im endlichen Falle hat man verschiede Alternativen, um Tupeln zu realisieren.)

## SKA 7-5 ##

Vgl. [Skript, **Bsp. 5.1.1, (5)**].

Ansatz I: direkt.

Ansatz II: arbeite mit Basen.

## SKA 7-6 ##

.

## SKA 7-7 + 12 ##

Seien $m,n\in\mathbb{N}$ und $A\in M_{m\times n}(K)$ eine Matrix und $b\in K^{m}$. Und wir betrachten das LGS $Ax = b$.

Homogener Lösungsraum: $V:=\{x\in K^{n}\mid Ax=\mathbf{0}\}$.
Zu zeigen: $V$ ist ein Untervektorraum.

- **Zz**: $V\neq\emptyset$. Das gilt weil $\mathbf{0}\in V$, weil $A\mathbf{0}=\mathbf{0}$.
- Seien $u,v\in V$ und $\alpha\in K$. **Zz:** $\alpha u+v\in V$.
Es gilt
$A(\alpha u+v)=\alpha Au+Av=\alpha\cdot\mathbf{0}+\mathbf{0}=\mathbf{0}$, weil $u,v\in V$. Also gilt $\alpha u+v\in V$ per Konstruktion.



Lösungsraum: $W:=\{x\in K^{n}\mid Ax=b\}$.
Zu zeigen: $W$ ist ein affiner Unterraum.

- Wenn $Ax=b$ keine Lösung hat, dann gilt $W=\emptyset$ und damit ist $W$ per Definition affin.
- Wenn $Ax=b$ eine Lösung hat... Fixiere eine Lösung $x_{0}\in K^{n}$. Darum gilt

    $\begin{array}{rcl}
        W &= &\{x\in K^{n}\mid x=u+x_{0},\, Au=\mathbf{0}\}\\
        &= &\{x\in K^{n}\mid x=u+x_{0},\, u\in V\}\\
        &= &x_{0}+V\\
    \end{array}$

    Also ist $W$ die Summe aus einem Vektor und einem linearen Unterraum (siehe A7-7). Darum ist $W$ affin.






## SKA 7-8 ##

.

## SKA 7-9 ##

Vgl. [Skript, **Lemma 5.1.6**].

**Behauptung.** Seien $V$ ein Vektorraum über $K$
und $U_{i}\subseteq V$ Untervektorräume. Dann ist $U:=\bigcap_{i\in I}U_{i}\subseteq V$ wiederum ein Untervektorraum.

**Beweis.**
Wir gehen die Axiome durch:

(NL) Beachte, dass $0\in U_{i}$ für alle $i\in I$.
Darum $0\in\bigcap_{i\in I}U_{i}=U$.
Insbesondere ist $U$ nicht leer.

(LK) Seien $\alpha,\beta\in K$ und $u,v\in U$.
**Zz:** $\alpha u+\beta v\in U$.

Sei $i\in I$. Dann $u,v\in U_{i}$.
Da $U_{i}$ ein UVR ist, gilt $\alpha u+\beta v\in U_{i}$.

Da das für alle $i\in I$ gilt, gilt
$\alpha u+\beta v\in \bigcap_{i\in I}U_{i}=U$.

Darum ist $U$ ein UVR.
**QED**


## SKA 7-10 ##

.
## SKA 7-11 ##

.
## SKA 7-13 ##

.
## SKA 7-14 ##

.
## SKA 7-15 ##

.
## SKA 7-16 ##


```
1 5 3 0
2 4 0 0
2 4 0 0

1 # # | 0
0 0 1 | 0
0 0 0 | 0

==> alle Lösungen sind der Form (0, 0, t), wobei t frei

==> linear abhängig
```