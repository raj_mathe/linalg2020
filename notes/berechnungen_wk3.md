# Kritzelei aus Woche 3 #

## Übungsblatt 1 ##

Für volle Lösungen siehe Datei [/docs/loesungen.pdf](../docs/loesungen.pdf).
### Anmerkung zu Aufgabe 2 ###

Seien **A** eine m x n Matrix über IR, und **b** in IR^m.

_Lösungsmenge vor Transformation:_

Sei L_1 := { x ∈ IR^n | Ax = b }

_Lösungsmenge nach Transformation:_

Sei L_2 := { x ∈ IR^n | A'x = b' },
wobei (A'|b') das Resultat einer Transformation (Art I, II, III) ist.

**BEHAUPTUNG.** Es gilt L_1 = L_2.

**BEWEIS.**

- **Zu zeigen 1:** L_1 ⊆ L_2
    - Sei x aus L_1 beliebig. D. h. **x** ist eine Lösung zu (A|b)
    - **Zu zeigen:** x in L_2, d. h. dass x eine Lösung zu (A'|b') ist.
        - Fall 1. Transformation vom Typ I:
            - ...
        - Fall 2. Transformation vom Typ II:
            - ...
        - Fall 3. Transformation vom Typ III:
            - ...

- **Zu zeigen 2:** L_2 ⊆ L_1
    - Sei x aus L_2 beliebig. D. h. **x** ist eine Lösung zu (A'|b')
    - **Zu zeigen:** x in L_1, d. h. dass x eine Lösung zu (A|b) ist.
        - _Unvollständige Argumentation:_ Die Transformationen sind umkehrbar. Also ist x eine Lösung von (A|b) auch.
            - !! **Fehlt:** Warum bedeutet diese Umkehrbarkeit, dass x noch eine Lösung von (A|b) ist? !!
        - Richtiger Ansatz 1:
            - Gegeben ist, dass A'x = b' gilt.
            - Nun gilt: A' = E·A, b' = E·b, wobei E die Zeilenumformung ist.
            - **Umkehrbarkeit der Transformation bedeutet:** E ist umkehrbar.
            - Also, aus A'x = b' (d. h. E·A·x = E·b) folgt Ax = b.
        - Richtiger Ansatz 2:
            - (A'|b') entsteht durch Anwendung von I, II, od. III. aus (A|b)
            - **die Umkehrung (von (A'|b') ---> nach (A|b)) ist selbst eine Transformation vom Typ I, II, od. III.**
            - Also (A|b) ist eine Transformation von (A'|b')
            - Der erste Teil des Beweis hat gezeigt, dass
                - **x** Lösung von (A'|b') ==> **x** Lösung von Transformation von (A'|b')
                - d. h. **x** Lösung von (A'|b') ==> **x** Lösung von Transformation von (A|b).

**QED**
