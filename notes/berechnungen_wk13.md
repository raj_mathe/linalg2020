# Woche 13 #

## Bestimmung von invertierbaren Elementen und ihren Inversen ##

Wir benutzen das Ergebnis

    k invertierbar in ℤ/n
    ⟺ ggT(k, n) = 1
    ⟺ k, n teilerfremd

### Beispiel 1. ###

In ℤ/10:

    k               | 0 1 2 3 4 5 6 7 8 9
    k invertierbar? | x √ x √ x x x √ x √

invertierbare Elemente: {1, 3, 7, 9}.

### Beispiel 2. ###

In ℤ/p sind alle Elemente außer 0 invertierbar. Wir berechnen die Inversen durch Ausprobieren
und wir beachten

- 0 hat kein Inverses
- 1 invertiert sich selbst
- für jedes x ≠ 0
    - x invertiert sich selbst, oder
    - ∃y ≠ x, so dass x, y einander invertieren.

ℤ/2

    k   | 0 1
    k¯¹ | - 1

ℤ/3

    k   | 0 1 2
    k¯¹ | - 1 2

ℤ/5

    k   | 0 1 2 3 4
    k¯¹ | - 1 3 2 4

ℤ/7

    k   | 0 1 2 3 4 5 6
    k¯¹ | - 1 4 5 2 3 6

Den Vorgang des Ausprobieren können wir für
ℤ/n verwenden, auch wenn n keine Primzahl ist.
Es gibt nur 3 statt 2 Möglichkeiten:
- x nicht invertierbar
- x invertiert sich selbst
- x invertiert durch ein y (und y invertiert durch x).

ℤ/4

    k   | 0 1 2 3
    k¯¹ | - 1 - 3

ℤ/6

    k   | 0 1 2 3 4 5
    k¯¹ | - 1 - - - 5
