# Kritzelei aus Woche 4 #

## SKA 3 ##

- **1.** Unterschied zw. ℕ, ℕ₀ beachten.
- **2.** „minimales Beispiel“: A = {🍎}, B = Ø, C = Ø.
- **3.** x ∈ linker Seite ⟺ x ∈ rechter Seite; Dualität zw. Mengen und logischen Operationen.
- **4.** ja —> Aussagenlogischer Ansatz vs. „visueller“ Ansatz vs. „algebraischer“ Ansatz (DeM).
- **5.** =
- **6.** 3·4, ja
- **7.** erst Z in R „definieren“, dann ZxZ in RxR definieren, analog mit NxN ⊆ ZxZ
- **8.** Diagramm
- **9.** ja
- **10.** ∈: nein, ⊆: ja 
- **11.** Formale Semantik / algebraische Oeprationen
- **12.** nein, sondern sind klassische Komplemente
- **13.** Mengentheoretisch: Ja, weil Gph(ƒ) = Gph(g). Kategorientheoretisch: „Nein“.
- **14.** Fasern/Bildmengen für ƒ : X ⟶ Y

        ƒ Injektiv ⟺ alle Fasern von ƒ enthalten ≤ 1 Element
        ƒ Surjektiv ⟺ alle Faster von ƒ sind nicht leer ⟺ ƒ(X) = Y
- **15.** ƒ¯¹{y} ist die Schnittmenge aus Gph(ƒ) und dem Geraden {(x,y) | x ∈ ℝ}

        Injektiv ⟺ jede Schnittmenge von Gph(ƒ) mit vertikalen Geraden hat höchstens 1 Pkt
        Surjektiv ⟺ jede Schnittmenge von Gph(ƒ) mit vertikalen Geraden hat mindestens 1 Pkt
- **16.** dom(log) = (0,∞), ran(log) = ℝ


## SKA 4 ##

- **1.** Lösungsskizze:

        R := Gph(ƒ). Etwas ausführlicher:

        ƒ : X ⟶ Y sei eine Funktion
        R := {(x,y) ∈ X x Y | ƒ(x) = y} = Gph(ƒ)
        Dann ist R eine binäre Relation mit R ⊆ X x Y

- **2.** Lösungsskizze

        Sei ƒ : M ⟶ N definiert durch
        ƒ(m) = das n, so dass (m,n) ∈ R
        für alle m ∈ M.

        (i) ⟺ ƒ überall definiert;
        (ii) ⟺ ƒ wohldefiniert

- **3.** Beachte, dass die Relation auf P(X) ist und _nicht_ auf X!

        Formales Argument
        ~~~~~~~~~~~~~~~~~
        Wir prüfen die Axiome einer OR:
        Refl.       Zz: Sei A ∈ P(X). Dann A ≤ A.
                    Offensichtlich gilt X \ A ⊆ X \ A.
                    Per Konstruktion gilt also A ≤ A.
        Antisymm.   Zz: Seien A, B ∈ P(X). Dann A ≤ B und B ≤ A ⟹ A=B.
                    Es gilt
                        A ≤ B und B ≤ A.
                        ⟹ X \ A ⊆ X \ B und X \ B ⊆ X \ A
                            per Konstruktion
                        ⟹ X \ A = X \ B
                            per Definition von Mengengleichheit
                        ⟹ X \ (X \ A) = X \ (X \ B)
                        ⟹ A = B
                            da A, B Teilmengen von X sind
        Trans.      Zz: Seien A, B, C ∈ P(X). Dann A ≤ B und B ≤ C ⟹ A ≤ C.
                    Es gilt
                        A ≤ B und B ≤ C.
                        ⟹ X \ A ⊆ X \ B und X \ B ⊆ X \ C
                            per Konstruktion
                        ⟹ X \ A ⊆ X \ C
                            da Mengeninklusion transitiv ist
                        ⟹ A ≤ C
                            per Konstruktion.
        Also genügt (P(X), ≤) den Axiomen einer OR.

- **4.** Beachte: Entfernung von P(C) nicht von C!!

        Lösung
        ~~~~~~~~
        Entferne Ø von P(C).
        Dann existiert kein „kleinstes Element“ (auch „Minimum“ genannt).
        Allerdings existieren genau 3 „minimale Elemente“ in (P(C)\{Ø}, ⊆), viz. {a}, {b}, {c}.
- **5.** Ja in beiden Fällen (im 2. Falle nehmen wir an, dass Alle Wörter mindestens 2 Buchstaben enthalten).

        Formales Argument:
        ~~~~~~~~~~~~~~~~~~
        Sei ∑ die Menge von Buchstaben und W die Menge von Wörtern im Wörterbuch.
        Dann handelt es sich in beiden Fällen um eine Relation, die durch

            ~ := {(w1,w2) ∈ W⨉W | ƒ(w1) = ƒ(w2)}

        definiert wird, wobei ƒ eine Abbildung von W nach ∑ ist.
        (Im 1. Falle gilt ƒ(w) = erster Buchstabe in w;
        im 2. Falle gilt ƒ(w) = zweitletzter Buchstabe in w.)

        Schnelle Variante:
        ~~~~~~~~~~~~~~~~~~
            (1) Für w1, w2 ∈ W gilt w1 ~ w2 ⟺ ƒ(w1) = ƒ(w2).
                D. h. ƒ ist eine „Reduktion“ von der ÄR (W, ~) auf (∑, =).
            (2) (∑, =) ist eine ÄR, d.h. Gleichheit ist eine Äquivalenzrelation auf ∑.
            (3) Aus (1) + (2) folgt, dass ~ eine ÄR ist.

        Ausführliche Variante:
        ~~~~~~~~~~~~~~~~~~~~~~
            Wir prüfen die Axiome einer OR:
            Refl.       Zz: Sei w ∈ W. Dann w ~ w.
                        Es gilt ƒ(w) = ƒ(w), da „=“ reflexiv ist.
                        Per Konstruktion gilt also w ~ w.
            Symm.       Zz: Seien u, v ∈ W. Dann u ~ v ⟹ v ~ u.
                        Es gilt
                            u ~ v
                            ⟹ ƒ(u) = ƒ(v) per Konstruktion
                            ⟹ ƒ(v) = ƒ(u) da „=“ symmetrisch ist
                            ⟹ v ~ u per Konstruktion.
            Trans.      Zz: Seien u, v, w ∈ W. Dann u ~ v und v ~ w ⟹ u ~ w.
                        Es gilt
                            u ~ v und v ~ w
                            ⟹ ƒ(u) = ƒ(v) und ƒ(v) = ƒ(w) per Konstruktion
                            ⟹ ƒ(u) = ƒ(w) da „=“ transitiv ist
                            ⟹ u ~ w per Konstruktion.

            Also genügt (W, ~) den Axiomen einer ÄR.

- **6.** -
- **7.** -

- **8.** Schubfachprinzip mit 4 Kategorien und 5 Plätzen:

        Schnelles Argument:
        ~~~~~~~~~~~~~~~~~~~
        muss gelten, da sonst jede Farbe höchstens 1 Mal vorkommt,
        was höchstens 4 Plätze belegt, aber wir wählen 5 Karten.

- **9.** Schubfachprinzip mit 366 Kategorien und 7000 Plätzen:

        Schnelles Argument:
        ~~~~~~~~~~~~~~~~~~~
        Falls für jeden Tag max 17 Studierende diesen Geburtstag haben,
        dann würde es maximal
            (18-1)·366 = 6222
        Studierende geben.
        Aber es gibt 7000 (> 6222) Studierende.
        Widerspruch!
        Darum gibt es einen Tag, an dem (mind.) 18 Studierende den als ihren Geburtstag feiern.

        Formales Argument:
        ~~~~~~~~~~~~~~~~~~
        Sei T die Menge von Tagen. Also |T|=366
        Sei S die Menge von Studierenden, |S|≥7000.
        Sei
            ƒ : S ⟶ T
        die Funktion, die jedem Studierenden seinen Geburtstag zuordnet.
        Setze
            G := {ƒ¯¹{d} | d ∈ T} \ {Ø}.
        und
            geb : G ⟶ T
        durch
            geb(A) = ƒ(a) für ein a ∈ A
        für jedes A ∈ G.

            Beobachtung 1:
            ~~~~~~~~~~~~~~
            Die Funktion, geb, ist wohldefiniert:

            Sei A ∈ G beliebig.
            Dann A = ƒ¯¹{d} für ein d ∈ T und A ≠ Ø
            Also gibt es ein a ∈ A
            und weiterhin gilt für a1, a2 ∈ A, dass ƒ(a1) = d = ƒ(a2).
            Darum ordnet geb der Menge A exakt einen Wert zu.

            Beobachtung 2:
            ~~~~~~~~~~~~~~
            Die Funktion, geb, ist injektiv:

            Seien A1, A2 ∈ G.
            Zz: ƒ(A1) = ƒ(A2) ⟹ A1 = A2.
            Per Konstruktion gelten
                A1 = ƒ¯¹{d1}, A1 ≠ Ø, und
                A2 = ƒ¯¹{d2}, A2 ≠ Ø
            für ein d1, d2 ∈ T.
            Wie oben gilt ƒ(A1) = d1 und ƒ(A2) = d2.
            Darum
                ƒ(A1) = ƒ(A2)
                ⟹ d1 = d2
                ⟹ ƒ¯¹{d1} = ƒ¯¹{d2}
                ⟹ A1 = A2.

            Beobachtung 3:
            ~~~~~~~~~~~~~~
            Es gilt S = ⋃{A | A ∈ G}.
            Warum?
            - Per Konstruktion gilt A ⊆ S für alle A ∈ G.
                Also gilt ⋃{A | A ∈ G} ⊆ S.
            - Sei s ∈ S belibig.
                Seien d := ƒ(s) und A₀ := ƒ¯¹{d}.
                Dann A₀ ≠ Ø, da s ∈ A₀, da ƒ(s) = d.
                Also gilt A₀ ∈ G per Konstruktion von G.
                Also s ∈ A₀ ⊆ ⋃{A | A ∈ G}.
                Darum gilt S ⊆ ⋃{A | A ∈ G}.

        Da die Funktion, geb, injektiv ist (Beobachtung 2),
        liefert das SCHUBFACHPRINZIP
            |G| ≤ |T| = 366.
        Da die Mengen in G offensichtlich paarweise disjunkt sind,
        und da S = ⋃{A | A ∈ G} (Beobachtung 3),
        gilt
            7000 ≤ |S|
                 = ∑{|A| | A ∈ G}
                 ≤ max{|A| | A ∈ G} · |G|
                 ≤ max{|A| | A ∈ G} · 366.
        Also
            max{|A| | A ∈ G} ≥ 7000/366 > 19.
        Also existiert mindestens ein A₀ ∈ G mit |A₀| > 19 > 18.
        Per Konstruktion von G haben nun alle Studierende in A₀ den gleichen Geburtstag.
        Darum haben mindestens 18 Studierende denselben Geburtstag.

- **10.**

        Induktionsargument:
        ~~~~~~~~~~~~~~~~~~~
        IND-ANFANG:
        Für n = 1. Nichts zu zeigen, da ∏{E_i : 1≤i≤1} = E_1.
        Für n = 2.
            ... siehe Argument im Skript
            ... oder gilt einfach per Definition: siehe jedes Lehrbuch über Mengenlehre.
        Sei n > 2.
        IND-VORAUSSETZUNG:
            Angenommen, |∏{E_i : 1≤i<n}| = ∏{|E_i| : 1≤i<n}.
        IND-SCHRITT:
            Es gilt
            |∏{E_i : 1≤i≤n}|
                = |∏{E_i : 1≤i<n} ⨉ E_n| wegen bijektiver Äquivalenz
                = |∏{E_i : 1≤i≤n}|·|E_n| aus (allgemeinem) Fall n=2
                =(∏{|E_i| : 1≤i<n}·|E_n| per Induktionsvoraussetzung
                = ∏{|E_i| : 1≤i≤n},
        Darum gilt die Aussage per Induktion.

- **11.** Induktionsschritt (n —> n+1) geht nur, wenn n ≥ 2.
    Das heißt, der Fall 1 —> 2 wird übersprungen.
