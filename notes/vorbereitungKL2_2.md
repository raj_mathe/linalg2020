## Lineare Ausdehnung ##

Aufgabe 5b aus Klausur

    i)

        v1=... w1=...
        v2=... w2=... wie in Aufgabe

        Wähle v3 = (1 0 0)
        Oder sage: „es gibt“ ein v3, so dass {v1,v2,v3} eine Basis von R^3 ist

        Wähle w3 in R^3 beliebig
        ⟹ ex. lin Abb φ : R^3 ⟶ R^3 (siehe Satz 6.1.13)
        mit
            φ(v1) = w1
            φ(v2) = w2
            φ(v3) = w3

    ii) Wir wissen, dass {w1, w2} lin unabh.
        - also ex. w3 ∈ R^3 s. d. {w1, w2, w3} eine Basis von R^3 ist.
        - lin Abb φ : R^3 ⟶ R^3 wie vorher erzeugen.
        - bleibt zu zeigen, dass φ ein Isomorphismus ist.

        Zz: φ ist injektiv.
            [Dann folgt: φ bijektiv (weil VR beide 3-dimensional sind), also φ ein Isomorphismus.]
        Sei also x ∈ Kern(φ).
            Dann x = c1·v1 + c2·v2 + c3·v3
            Also 0 = φ(x) = c1·w1 + c2·w2 + c3·w3
            Also c1, c2, c3 = 0, weil {w1, w2, w3} eine Basis
            Also x = c1·v1 + c2·v2 + c3·v3 = 0.
        ⟹ Damit haben wir gezeigt, dass Kern(φ) = {0}
            (beachte, dass 0 immer im Kern ist)
        ⟹ φ injektiv.

        ODER

        Aus Korollar 6.1.15 folgt φ ein Iso, weil {w1, w2, w3} eine Basis ist.

    iii) setze w3 = 0. Konstruiere φ wie oben.
        Dann erfüllt φ die erwünschten Eigenschaften.
        Und φ(v3) = w3 = 0, sodass Kern(φ) ≠ {0}, weil v3 ≠ 0.
        Darum ist φ nicht injektiv und damit kein Isomorphismus.

**Empfehlung:** Mache _Übungsblatt 9 Aufgabe 2_!

## Zum Thema Rang <~~~> Inj/Surj

Wenn dim(W) = m, m eine endliche Zahl:

1.
        φ injektiv  ⟺ Kern(φ) = {0}
                    ⟺ dim(Kern(φ)) = 0
                    ⟺ dim(Bild(φ)) = dim(V)
                    ⟺ Rang(φ) = dim(V)
                    ⟺ Rang(φ) ≥ dim(V)

2.

        φ surjektiv ⟺ Bild(φ) = W
                    ⟺ dim(Bild(φ)) = dim(W) (=m)
                    ⟺ Rang(φ) = dim(W)
                    ⟺ Rang(φ) ≥ dim(W)

Der Punkt? Wir können Rang(φ) _berechnen_.

Anwendung: z. B. wenn Bild(φ) = lin{w1, w2, ..., w_r} und {w1, w2, ..., w_r} lin unabh,
dann gilt offensichtlich dim(Bild(φ)) = r.

Und falls wir nicht wissen, ob {w1, w2, ..., w_r} lin unabh ist,
dann wissen wir dennoch mindestens, dass dim(Bild(φ)) ≤ r,
weil wir eine Teilmenge aus ≤r Vektoren finden können,
die eine Basis für Bild(φ) bilden.

## MATRIZEN ##

Matrizen werden mal so in Bezug auf ihre Einträge folgendermaßen formal dargestellt:

    A = ( a_ij ) eine m x n Matrix
    B = ( b_ij ) eine m x n Matrix

Mit dieser Darstellung kann man dann Ergebnisse von algebraischen Operationen analog darstellen,
wie z. B.

    A + 5B = ( a_ij + 5b_ij ).

Seien

    A = ( a_ij ) eine m x n Matrix
                        ¯
    B = ( b_ij ) eine n x l Matrix
                  ¯
Zur Matrixmultiplikation müssen die „innere Dimensionen“ übereinstimmen,
um die Operation auszuführen (wenn die quadratisch sind, dann gilt das ohnehin).
Es gilt

                             n
    A·B = ( c_ij ), wobei c_ij = ∑ a_ik b_kj
                            k=1

Hingegen (solange m=l) gilt

                                 l
    B·A = ( d_ij ), wobei d_ij = ∑ b_ik a_kj
                                k=1



## BEWEISE ##

### Übungsblatt 3 Aufgabe 2d) ###

    Behauptung. A,B ⊆ Y gilt f^-1(A∩B) = f^−1(A) ∩ f^−1(B).

    Beweis.
    (⊆) Sei x ∈ f^-1(A∩B) beliebig.
        Zu zeigen: x ∈ f^−1(A) ∩ f^−1(B).
        D. h. wir müssen zeigen,
        dass x ∈ f^−1(A) und x ∈ f^−1(B).

        Es gilt

            x ∈ f^-1(A∩B)
            ⟹ f(x) ∈ A ∩ B                 (per Definition von f^-1)
            ⟹ f(x) ∈ A und f(x) ∈ B
            ⟹ x ∈ f^-1(A) und x ∈ f^-1(B)  (per Definition von f^-1)

        Darum gilt x ∈ r. S.


    (⊇) Sei x ∈ f^−1(A) ∩ f^−1(B).
        D. h. x ∈ f^−1(A) und x ∈ f^−1(B).
        Zu zeigen: x ∈ f^-1(A∩B).

        Es gilt

            x ∈ f^-1(A) und x ∈ f^-1(B)
            ⟹ f(x) ∈ A und f(x) ∈ B   (per Definition von f^-1)
            ⟹ f(x) ∈ A ∩ B
            ⟹ x ∈ f^-1(A∩B)           (per Definition von f^-1)

        Darum gilt x ∈ l. S.

    QED.

### Übungsblatt 9 Aufgabe 3 ###

Es seien U, V und W Vektorräume über einem Körper K.
Seien φ: U ⟶ V und ψ : V ⟶ W lineare Abbildungen.

    Beh. ψ ◦ φ injektiv ⟺ (φ injektiv ist + Kern(ψ) ∩ Bild(φ) = {0}).

    Beweis.
        (⟹) Angenommen, ψ ◦ φ injektiv.
            Zu zeigen:
            i)  φ injektiv
            ii) Kern(ψ) ∩ Bild(φ) = {0}.

            Zu i): Zu zeigen: Kern(φ) = {0}.
            Sei also x ∈ U mit φ(x) = 0.
            Dann (ψ ◦ φ)(x) = ψ(φ(x)) = ψ(0) = 0.
            Also x ∈ Kern(ψ ◦ φ) und per ANNAHME Kern(ψ ◦ φ) = {0} (weil injektiv).
            Also x = 0.
            Darum haben wir gezeigt, dass Kern(φ) ⊆ {0}.
            Also Kern(φ) = {0} (weil 0 immer im Kern ist).

            Zu ii): Zu zeigen Kern(ψ) ∩ Bild(φ) ⊆ {0} ( ⊇  gilt immer, weil 0 immer im Kern und Bild ).
            Sei also x ∈ Kern(ψ) ∩ Bild(φ).
            Zu zeigen: x = 0.
            Also x ∈ Kern(ψ) und x ∈ Bild(φ).
            Also ψ(x) = 0 und x = φ(y) für ein y ∈ U.
            Also ψ(φ(y)) = 0.
            Also y ∈ Kern(ψ ◦ φ) und per ANNAHME Kern(ψ ◦ φ) = {0} (weil injektiv).
            Also y = 0.
            Also x = φ(y) = φ(0) = 0.

        (⟸) Angenommen,
            i)  φ injektiv
            ii) Kern(ψ) ∩ Bild(φ) = {0}
            Zu zeigen: ψ ◦ φ injektiv.

            Es reicht also aus zu zeigen, dass
                Kern(ψ ◦ φ) = {0}.
            Sei also x ∈ U mit (ψ ◦ φ)(x) = 0.
            Zu zeigen: x = 0.
                ...
                ... [Annahme i + ii iwo gebrauchen]
                ...
            Also x = 0.
    QED
