# Woche 12 #

## Quiz 11  ##

Sei m = 4 und _A_ die folgende m x m Matrix über 𝔽₅:

    A = 1   2  -2  -1
        2   0  -1   1
        4   3   3   1
        1  -2   2   3

Zur Bestimmung der Invertierbarkeit führen wir das Gaußverfahren auf (A | I) aus:

        1   2  -2  -1 | 1   0   0   0
        2   0  -1   1 | 0   1   0   0
        4   3   3   1 | 0   0   1   0
        1  -2   2   3 | 0   0   0   1

    Zeile 2 <- Zeile 2 - 2·Zeile 1
    Zeile 3 <- Zeile 3 - 4·Zeile 1
    Zeile 4 <- Zeile 4 - Zeile 1


        1    2   -2   -1 |  1    0    0    0
        0   -4    3    3 | -2    1    0    0
        0   -5   11    5 | -4    0    1    0
        0   -4    4    4 | -1    0    0    1

    —> modulo 5

        1   2   3   4 | 1   0   0   0
        0   1   3   3 | 3   1   0   0
        0   0   1   0 | 1   0   1   0
        0   1   4   4 | 4   0   0   1

    Zeile 4 <- Zeile 4 - Zeile 2

        1   2   3   4 | 1   0   0   0
        0   1   3   3 | 3   1   0   0
        0   0   1   0 | 1   0   1   0
        0   0   1   1 | 1   4   0   1

    (hier habe ich sofort mod 5 berechnet)

    Zeile 4 <- Zeile 4 - Zeile 3

        1   2   3   4 | 1   0   0   0
        0   1   3   3 | 3   1   0   0
        0   0   1   0 | 1   0   1   0
        0   0   0   1 | 0   4   4   1

⟹ Rang(A) = 4 = m

⟹ _A_ ist invertierbar

    Zeile 1 <- Zeile 1 - 2·Zeile 2

        1   0   2   3 | 0   3   0   0
        0   1   3   3 | 3   1   0   0
        0   0   1   0 | 1   0   1   0
        0   0   0   1 | 0   4   4   1

    Zeile 1 <- Zeile 1 - 2·Zeile 3
    Zeile 2 <- Zeile 2 - 3·Zeile 3

        1   0   0   3 | 3   3   3   0
        0   1   0   3 | 0   1   2   0
        0   0   1   0 | 1   0   1   0
        0   0   0   1 | 0   4   4   1

    Zeile 1 <- Zeile 1 - 3·Zeile 4
    Zeile 2 <- Zeile 2 - 3·Zeile 4

        1   0   0   0 | 3   1   1   2
        0   1   0   0 | 0   4   0   2
        0   0   1   0 | 1   0   1   0
        0   0   0   1 | 0   4   4   1

⟹ Das Produkt der Elementarmatrizen, die A auf I (linke Hälfte) reduziert hat,
steht nun in der rechten Hälfte:

        A¯¹ = 3   1   1   2
            0   4   0   2
            1   0   1   0
            0   4   4   1

## Lineare Ausdehnung  ##

**Aufgabe 1.**

Seien

    w1 = (1,  1, 0)ᵀ
    w2 = (1, -1, 2)ᵀ
    w3 = (0, 3, -1)ᵀ

    v1 = ( 2, 1)ᵀ
    v2 = (-1, 1)ᵀ
    v3 = ( 1, 0)ᵀ

Gibt es eine lineare Abbildung, φ : ℝ³ —> ℝ²,
so dass

    φ(w1) = v1
    φ(w2) = v2
    φ(w3) = v3

gilt? Ist dies injektiv/surjektiv/bijektiv?

**Antwort.**
{w1, w2, w3} eine Basis
    ~~~> Gaußverfahren auf (w1 w2 w3) und Rang berechnen (soll gleich 3 sein)!
==> ja! (Satz 6.1.13 aus VL)

- Nicht injektiv, weil Rang(φ) <= 2, aber 2 ≥ 3 gilt nicht.
- Surjektiv:
    Zz: Rang(φ) ≥ 2.
    φ = φ_A
    A = Darstellungsmatrix
        ....
- Bijektiv: nein, weil nicht injektiv.

**Aufgabe 2.**

Seien

    w1 = (1,  1, 0)ᵀ
    w2 = (1, -1, 2)ᵀ

    v1 = ( 2, 1)ᵀ
    v2 = (-1, 1)ᵀ

Gibt es eine lineare Abbildung, φ : ℝ³ —> ℝ²,
so dass

    φ(w1) = v1
    φ(w2) = v2

gilt, und so dass φ injektiv ist? surjektiv? bijektiv?

**Antwort.**

- {w1, w2} sind linear unabhängig (wiederum mit Gaußverfahren und Rang zeigen).
- {w1, w2} können zu einer Basis von ℝ³ ergänzt werden: {w1, w2, w3}
- Setze v3 ∈ ℝ² beliebig
    - Satz der linearen Ausdehnung (6.1.13) wieder anwenden:
    - _es gibt eine_ lineare Abb, φ, die

            φ(w1) = v1
            φ(w2) = v2
            φ(w3) = v3

    erfüllt.
    - Bild(φ) = lin{v1,v2,v3} ⊇ lin{v1, v2} = ℝ²,
        weil {v1, v2} eine Basis von ℝ².
        Also Bild(φ) = ℝ².
    - Darum ist φ surjektiv.
- Es gibt keine injektive (und damit keine bijektive) lin. Abb. φ von ℝ³ nach ℝ²,
weil Rang(φ) <= 2, und 2 ≥ 3 gilt nicht.

**Aufgabe 3a.**

Seien

    w1 = (1,  1, 0)ᵀ
    w2 = (1, -1, 1)ᵀ
    w3 = (2, 0, 1)ᵀ

    v1 = ( 2, 1, 0)ᵀ
    v2 = (-2, 1, 0)ᵀ
    v3 = (1, 2, 0)ᵀ

Gibt es eine lineare Abbildung, φ : ℝ³ —> ℝ³,
so dass

    φ(w1) = v1
    φ(w2) = v2
    φ(w3) = v3

gilt, und so dass φ injektiv ist? surjektiv? bijektiv? nicht injektiv?

**Antwort.**

Beachte, {w1, w2, w3} ist nicht linear unabh.
Es gilt
- {w1, w2} linear unabh
- w3 ∈ lin{w1, w2}, und zwar w3 = w1 + w2
- Aber v3 = v1 + v2.

Darum ist die Frage äquivalent zu derselben Frage, nur
mit nur den ersten 2 Bedingungen, weil die 3. immer mit erfüllt sein wird,
weil falls φ : ℝ³ —> ℝ³ linear und Bed. 1+2 erfüllt,
so gilt Bedingung 3, weil

    φ(w3) = φ(w1 + w2) = φ(w1) + φ(w2) = v1 + v2 = v3.

Ansatz:

- füge w3' hinzu, damit {w1,w2,w3'} eine Basis von ℝ³ ist.
- v3' jetzt so wählen, dass φ injektiv/nicht injektiv ist.
- Beachte Korollar 6.1.11 im besonderen Falle dass φ : ℝⁿ —> ℝⁿ mit gleicher Dim für Inputraum und Outputraum!! Lin Abb. injektiv ⟺ surjektiv ⟺ bijektiv (≡ „Isomorphismus“).


**Aufgabe 3b.**

Seien

    w1 = (1,  1, 0)ᵀ
    w2 = (1, -1, 1)ᵀ
    w3 = (2, 0, 1)ᵀ

    v1 = ( 2, 1, 0)ᵀ
    v2 = (-1, 1, 0)ᵀ
    v3 = (1, 4, 0)ᵀ

Gibt es eine lineare Abbildung, φ : ℝ³ —> ℝ³,
so dass

    φ(w1) = v1
    φ(w2) = v2
    φ(w3) = v3

gilt, und so dass φ injektiv ist? surjektiv? bijektiv?

**Antwort.**

Beachte, {w1, w2, w3} ist nicht linear unabh.
Es gilt
- {w1, w2} linear unabh
- w3 ∈ lin{w1, w2}, und zwar w3 = w1 + w2
- Aber v3 ≠ v1 + v2.

Darum kann es niemals eine lineare Abb geben, die alle 3 Bedingungen erfüllt,
weil falls φ : ℝ³ —> ℝ³ linear ist und Bed. 1+2+3 erfüllt,
so gilt

    φ(w3) = φ(w1 + w2) = φ(w1) + φ(w2) = v1 + v2 ≠ v3.

D. h. Bedingung 3 wäre verletzt.


**TODO** Die o. s. Varianten (die letzteren) ausrechnen und hochladen.
