# Allgemeine Tipps #

- Vor der Klausur gut ausschlafen, denn sonst wird deine Wahrnehmung (+ Konzentration) beeinträchtigt.
- Vor der Klausur ein eigene Zusammenfassung als Dokument od. Checkliste o. Ä. erstellen:
    - Hauptresultate (z. B. Dimensionsformel, wichtige Lemmata wie „φ inj ⟺ Κern(φ)={0}“) aufschreiben
    - od. Stellen im Skript „verlinken“ (Seite / Referenznr, damit du schnell aufschlagen kannst, um Details nachzuschlagen).
    - Tipps bzgl. häufigster zu vermeidender (v. a. persönlicher) Fehler, usw. aufschreiben.
- Habt ein gutes System parat, um schnell die Dokument hochzuladen, damit ihr ggf. auch die Pufferzeit ausnutzen könnt.
- Aufgabe auf getrennten Blättern.
- Zeit aufteilen (z. B. 15 Minuten pro Aufgabe)
    - 2 Minuten sorgfältig durchlesen
    - (+ 2–5 Min) für Beweise, auch wenn man keinen detaillierten Ansatz hat, aber mindestens die Frage genug versteht,
        fange schon an, die Textstruktur aufzuschreiben.
    - wenn man nicht weiter weiß, Blatt frei lassen, später zurückkommen.
    - Ideal: versuche, insgesamt für die Aufgabe max 10–12 Minuten zu gebrauchen, damit ein paar Minuten zur Kontrolle übrig bleibt.
- Während der Klausur auf die Zeit achten (aber dabei nicht in Panik geraten!).
- Bei nicht zu begründenden Aufgaben (z. B. 1. Teil) versuche zu einem Punkt zu kommen, wo du dir „genügend“ sicher bist,
dass deine Vorgehensweise richtig ist. Lass die super ausführliche Arbeit für später.
Da hier keine Minuspunkte verteilt werden, lieber ein Versuch, der 80% richtig ist, als kein / ein abgebrochener Versuch.
- Wenn du etwas konstruierst, das gewisse Bedingungen erfüllen sollen,
gib dich nicht damit zufrieden, wenn du es konstruiert hast.
Begründe, dass die Bedingungen erfüllt sind.
- Achte darauf: wenn man von einer Annahme (X) auf eine Konklusion (Y) schließen soll,
    - es soll eine klare Brücke von logisch-mathematischen Schlüssen von X nach Y geschrieben werden;
    - wenn man nicht überall ausführlich sein kann, dann stelle sicher, dass du mindestens bei dem **nicht trivialen Schritt** in diesem Weg
    ausführlich bist;
    - es soll auf jeden Fall _sichtbar_ sein, dass diese Kette von Schlüssen etwas mit dem Ausgangspunkt (X) zu tun haben,
    und allmählich in etwas mit dem Ziel (Y) zu tun haben.
    Z. B. in A6 in der Klausur musst man von eine Bedingung über φⁿ auf eine Bedingung über φ kommen.
    Wenn im Beweis nirgends etwas über φⁿ richtig gebraucht wurde, dann ist der Ansatz
    womöglich ungültig, unvollständig, oder schlimmer: irrelevant zur Aufgabe.

## Beispiele über Modulararithmetik ##

Siehe [notes/berechnungen_wk13.md](./berechnungen_wk13.md), sowie Quiz 6.

## Gleichungssysteme ##

Siehe Quiz 1 und ÜB 1 Aufgabe 1.

## Beispiel mit A4 ##

Wenn man bei einer Aufgabe wie Aufgabe 4 in Klausur1 nicht weiter weiß,
kann man mindestens (1) sich Gedanken machen _Was soll ich hier beweisen?_;
und (2) die grobe Struktur des Beweises aufschreiben:

- Hauptbehauptung auf kleinere Teile reduzieren.
- Teile auf kleine „Ziele“ reduzieren.

Z. B.

```
Seien V, W VR über K.

Sei φ : V —> W eine Abb.

Behauptung:
​     φ linear ⟺ G ein lin. Teilraum von V x W

Beweis.
​    (⟹) Angenommen, φ sei linear.
                <---- [ ... Bemerkung: ich muss diese Annahme unten gebrauchen ... ]

​    ZU ZEIGEN: G ein lin. Teilraum, d. h.
​          (NL) G nicht die leere Menge.
​          (LK) G unter linearen Kombinationen stabil.

    Unsere neuen Ziele sind also:

​    ZU ZEIGEN 1: G nicht die leere Menge.
        Da φ(0) = 0, gilt (0, 0) ∈ G. Also G nicht leer.

        [ ... Bemerkung: jetzt machen, weil einfach ist ... ]

​    ZU ZEIGEN 2: G unter linearen Kombinationen stabil.
    Seien (v₁,w₁), (v₂,w₂) ∈ G und α₁, α₂ ∈ K.
    Wir müssen zeigen α₁·(v₁,w₁) + α₂·(v₂,w₂) ∈ G.


        [ ... Bemerkung: komme ggf. später auf Aufgabe zurück ... ]
        [ ... man sollte auch aufschreib, was (v₁,w₁) ∈ G überhaupt bedeutet in Bezug auf φ ... ]







​    (⟸) Angenommen, G ein lin. Teilraum.

    ZU ZEIGEN: φ linear, d. h.
​        (LIN) für alle v₁, v₂ ∈ V und c₁, c₂ ∈ K
            φ(c₁v₁ + c₂v₂) = c₁φ(v₁) + c₂φ(v₂)
    Seien also v₁, v₂ ∈ V und c₁, c₂ ∈ K.
    Setze
        w1 = φ(v₁)            }
        w2 = φ(v₂)            } <--- [ diese Aussagen können wir bzgl. G schreiben ]
        w3 = φ(c₁v₁ + c₂v₂)   }
    Wir müssen zeigen: w3 = c₁·w1 + c₂·w2.

    [ ... Bemerkung: komme ggf. später auf Aufgabe zurück ... ]
    [ ... da G in der Annahme ist, sollte man die Aussage auf eine Aussage in Bezug auf G transformieren ... ]




QED
```

## Aufgabe 5a ##

```
Input / Outputvektoren:
v1 = (-1 1)ᵀ     w1=(2 3)ᵀ
v2 = (-1 2)ᵀ     w2=(2 -2)ᵀ
v3 = (0 -1)ᵀ     w3=(0 5)ᵀ

BEDINGUNGEN:
1) φ(v1) = w1
2) φ(v2) = w2
3) φ(v3) = w3
```

Lösung

```
EXISTENZ
~~~~~~~~

{v1, v2} ist eine Basis, weil lin. unabh. und dim(R^2)=2
(—> schnelles Argument mit Gaußverf.)

Laut Satz über lin. Ausd. im Skript (6.1.13) ex. eine (eindeutige) lin. Abb, die φ(v1)=w1 und φ(v2)=w2 erfüllt.
​          (—> wegen der Eindeutigkeit im Satz gibt es insbes. maximal eine lineare Abbildung, die alle 3 Bedingungen erfüllt)

Wir müssen prüfen, dass φ(v3) = w3 (automatisch) mit erfüllt ist.
Beobachten: v3 = v1 – v2 also wegen Linearität

​     φ(v3) =  φ(v1) – φ(v2) = w1 – w2 = (0 5)ᵀ = w3

Darum ex. ein lin. Abb, φ, die alle 3 Bedingungen erfüllt,
nämlich die eben konstruirte Abbildung.
```

```
EINDEUTIGKEIT
~~~~~~~~~~~~~
Angenommen, ψ sei eine lin. Abb., die Bedingungen 1)–3), erfüllt.
Dann laut des oben zitierten Satzes müssen ψ und φ übereinstimmen,
weil ψ, φ beide die ersten Bedingungen 1)+2) erfüllen
und der Satz eine eindeutige lineare Abbildung liefert.
```

```
ISOMORPHISMUS
~~~~~~~~~~~~~
Beobachte: φ ist eine lin. Abb. zw. V und V.

Also gilt
​    φ Isomorphismus
​    ⟺ φ lineare Abbildung + φ inj + φ surj
          per Definition von „Iso“

​    ⟺ φ surj
            weil φ linear und
            laut Korollar 6.1.11: φ inj ⟺ φ surjektiv

​    ⟺ Bild(φ) = V      [wegen Lemma 6.1.4]
    ⟺ dim(Bild(φ)) = dim(V)
​    ⟺ dim(Bild(φ)) ≥ dim(V) (**)

Darum reicht es aus, (**) ZU ZEIGEN.
Dafür müssen wir dim(Bild(φ)) berechnen (---> NEUES ZIEL)
Wir wissen, dass

​     Bild(φ) ⊇ lin{w1, w2, w3}

Nun ist {w1, w3} linear unabhängig Darum

​     dim(Bild(φ)) ≥ dim(lin{w1, w2, w3}) ≥ dim(lin{w1, w3}) = 2 wegen lin. Unabhängigkeit

Also haben wir (**) gezeigt.
Wie oben durch die doppelte Implikation erklärt wird,
haben wir gezeigt, dass φ ein Isomorphismus ist.
```

## Aufgabe 6 ##


A6a)

```
Behauptung:
    φ erfüllt [Bedingung] ⟺ φ nicht inv.
Beweis.
    Fixiere ein n ≥ 1, s. d. φ^n = 0 gilt.

​    ZU ZEIGEN: φ hat kein Inverses (d. h. es gibt lin Abb ψ, so dass ψ ￮ φ = φ ￮ ψ = id).
    Angenommen, nicht. D. h. φ hat ein Inverses (d. h. ist bijektiv).
    Weil die Komposition zweier Bijektionen wiederum eine Bijektion ist,
        ist φ^2 = φ ￮ φ bijektiv,
​        und φ^3 = φ^2  ￮ φ ebenfalls,
​        und φ^4 = φ^3  ￮ φ ebenfalls,
​        ...
​        Also per Induktion ist φ^n bijektiv.
​     Aber φ^n = 0, was nicht bijektiv ist.
​     Widerspruch!
​     Darum gilt die Annahme nicht.
​     Also ist φ nicht invertierbar.
QED
```

A6b)

```
Behauptung:
    Angenommen, φ erfüllt [Bedingung]
    + für ein v ∈ V gilt v ≠ 0 und φ(v) ≠ 0.
    Dann Kern(φ) ∩ Bild(φ) ≠ {0}.
```

```
BEOBACHTUNG: {0} immer ⊆ Kern(φ) und Bild(φ).
Also hier muss gezeigt werden:
    dass {0} ⊂ .... strikt,#
    d. h. dass es ein w ∈ V, w ≠ 0,
​    so dass w im SCHNITT liegt, d. h.
​       1) w ≠ 0,
​       2) φ(w) = 0,         <— „w im Kern“
       3) w = φ(u), u ∈ V   <— „w im Bild“
```

```
Beweis (von Behauptung):
​     Wir betrachten die Folge:

​          0 ≠ v,  0 ≠ φ(v), φ(φ(v)), φ(φ(φ(v))), …, φ^n(v) = 0,

​     Darum gibt es eine Übergangsstelle k, so dass

​          φᵏ¯¹(v) ≠ 0 und φᵏ(v) = 0.

​     Insbesondere gilt 2 ≤ k ≤ n, wegen der Voraussetzungen auf φ und v.

​     Wir wählen nun w := φᵏ¯¹(v).
     Per Konstruktion gelten

​          w = φᵏ¯¹(v) ≠ 0
​          φ(w) = φ(φᵏ¯¹(v)) = φᵏ(v) = 0

    Das sind 1) + 2) oben. Es bleibt noch 3) zu zeigen.
    Da k ≥ 2, ist φᵏ¯²(v) ein wohldefiniertes Element in V
    und
    ​    w = φᵏ¯¹(v) = φ(φᵏ¯²(v)).

    Also sind 1) + 2) + 3) erfüllt,
    und wir haben ein Element, w, im Kern(φ) und Bild(φ) gefunden,
    das ungleich 0 ist.

    Also Kern(φ) ∩  Bild(φ) ≠ {0}.
QED
```

A6c)

```
Finde φ so, dass φ [Bedingung] und φ ≠ 0 gellten.
Arbeite mit Matrizendarstellung.

​    A = ( 0  1 )
​        ( 0  0 )  [ ... geschicktes Ausprobieren: siehe Aufnahme ... ]

- Es gilt A ≠ 0
- Es gilt A² = 0, sodass A stark kontrahierend ist.

Wir haben durch diese Matrizendarstellung
somit eine lineare Abbildung konstruiert,
die beide Bedingungen erfüllt (und diese begründet).
```

A6d)

```
Finde φ so, dass φ nicht [Bedingung] und φ nicht inv.

BEOBACHTUNG: Wir zeigen damit dass der Umkehrschluss zu (a), also
​     φ nicht inv ⟹ φ erfüllt [Bedingung],
nicht gültig sei.

Arbeite mit Matrizendarstellung (weil lin. Abb mit Matrizen identifiert werden können).
    A = ( 1  1 )
​        ( 1  1 )  [ ... geschicktes Ausprobieren: siehe Aufnahme ... ]

- A hat lin. abh. Spalten, also nicht inv.
- A^n hat nur positive Einträge, also wird niemals A^n = 0 gelten für n ≥ 1.

Wir haben durch diese Matrizendarstellung
somit eine lineare Abbildung konstruiert,
die beide Bedingungen erfüllt (und diese begründet).
```
