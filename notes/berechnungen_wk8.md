# Woche 8 #

## Hinweise zu ÜB 8-1 ##

Als Beispiel nehme ich die linearen Unterräume:

    U₁ = {x ∈ ℝ⁴ | x₁ + 3·x₂ = 4·x₃ + x₄},
    U₂ = {x ∈ ℝ⁴ | x₁ = 5·x₂ + 2·x₃ + x₄}.

Sei x ∈ ℝ⁴. Dann gelten offensichtlich

- x ∈ U₁ ⟺ x₁ + 3·x₂ - 4·x₃ - x₄ = 0 ⟺ A₁x = 0,
- x ∈ U₂ ⟺ x₁ - 5·x₂ - 2·x₃ - x₄ = 0 ⟺ A₂x = 0,
- x ∈ U₁∩U₂ ⟺ (x₁ + 3·x₂ - 4·x₃ - x₄ = 0 und x₁ - 5·x₂ - 2·x₃ - x₄ = 0) ⟺ A₃x = (0, 0)ᵀ,

wobei

- A₁ = die 1 x 4 Matrix

        (1 3 -4 -1),

- A₂ = die 1 x 4 Matrix

        (1 -5 -2 1),

- A₃ = die 2 x 4 Matrix

        (1  3 -4 -1)
        (1 -5 -2  1).

1) Für U₁ haben wir also x ∈ U₁ ⟺ A₁x = 0.
   Nun ist A₁ bereits in Zeilenstufenform.
   Und hier sind x₂, x₃, x₄ frei.
   Das liefert uns erzeugende Elemente, indem wir diese jeweils auf 0 od. 1 setzen.

        u₁ = (-3, 1, 0, 0)ᵀ [hier setze man x₂=1, x₃=x₄=0]
        u₂ = ( 4, 0, 1, 0)ᵀ [hier setze man x₃=1, x₂=x₄=0]
        u₃ = ( 1, 0, 0, 1)ᵀ [hier setze man x₄=1, x₂=x₃=0]

   Diese sind offensichtlich linear unabhängig (wegen der disjunkten Ein und Ausschaltung von freien Variablen),
   und da nur x₂, x₃, x₄ in der allgemeinen Lösung frei sind, sind diese Vektoren erzeugend für U₁.
   Also ist {u₁, u₂, u₃} eine Basis für U₁.

2) analog für eine Basisberechnung für U₂. Man bekommt als Basis
   {v₁, v₂, v₃}, wobei

        v₁ = ( 5, 1, 0, 0)ᵀ
        v₂ = ( 2, 0, 1, 0)ᵀ
        v₃ = (-1, 0, 0, 1)ᵀ.

3) Für U₁∩U₂ gilt x ∈ U₁∩U₂ ⟺ A₃x = 0.
   In Zeilenstufenform wird A₃ zu

        A₃ ~~> (1 3 -4 -1)
               (0 8 -2 -2)

   Also sind x₃ und x₄ frei.
   Die Auflösung des LGS liefert uns erzeugende Elemente,
   indem wir die freien Variablen jeweils auf 0 od. 1 setzen:

        w₁ = (13/4, 1/4, 1, 0)ᵀ [hier setze man x₃=1, x₄=0]
        w₂ = ( 1/4, 1/4, 0, 1)ᵀ [hier setze man x₄=1, x₃=0]

   Wir können diese Vektoren beliebig skalieren.
   Es ist sinnvoll alles mit 4 zu multiplizieren und man erhält stattdessen:

        w₁ = (13, 1, 4, 0)ᵀ
        w₂ = ( 1, 1, 0, 4)ᵀ

   Diese sind offensichtlich linear unabhängig (wegen der disjunkten Ein und Ausschaltung von freien Variablen),
   und da nur x₃, x₄ in der allgemeinen Lösung frei sind, sind diese Vektoren erzeugend für U₁∩U₂.
   Also ist {w₁, w₂} eine Basis für U₁∩U₂.

4) Für U₁ + U₂ erhält man mithilfe der oben berechneten Basen

        U₁ + U₂ = Lin{u₁, u₂, u₃} + Lin{v₁, v₂, v₃}
                = Lin{u₁, u₂, u₃, v₁, v₂, v₃}

    Darum ist {u₁, u₂, u₃, v₁, v₂, v₃} erzeugend für U₁ + U₂.
    Diese Vektoren sind aber nicht unbedingt linear unabhängig,
    also längst keine Basis. Wir führen das Gaußverfahren darauf,
    um auf eine maximale linear unabhängige Teilmenge davon zu kommen:

        (u₁ | u₂ | u₃ | v₁ | v₂ | v₃)

           ( -3   4   1   5   2  -1 )
        =  (  1   0   0   1   0   0 ) · 3, + Z1
           (  0   1   0   0   1   0 )
           (  0   0   1   0   0   1 )

           ( -3   4   1   5   2  -1 )
        ~> (  0   4   1   8   2  -1 )
           (  0   1   0   0   1   0 ) · -4, + Z2
           (  0   0   1   0   0   1 )

           ( -3   4   1   5   2  -1 )
        ~> (  0   4   1   8   2  -1 )
           (  0   0   1   8  -2  -1 )
           (  0   0   1   0   0   1 ) · -1, + Z3

           ( -3   4   1   5   2  -1 )
        ~> (  0   4   1   8   2  -1 )
           (  0   0   1   8  -2  -1 )
           (  0   0   0   8  -2  -2 )

     Die Stellen der Stufen weisen auf die linear unabhängigen Vektoren hin:
     {u₁, u₂, u₃, v₁} sind linear unabhängig und {v₂, v₃} hängen davon ab.
     Darum gilt

            U₁ + U₂ = Lin{u₁, u₂, u₃, v₁},

    sodass wegen linearer Unabhängigkeit {u₁, u₂, u₃, v₁} eine Basis ist.
    Da aber dim(V) = 4 = Anzahl der Basiselemente von U₁ + U₂,
    erhalten wir

            U₁ + U₂ = V.

## Alternativ I für Teilaufgabe 8-1 (4) ##

Man braucht nach dem Gaußverfahren keine Basiselemente aufzulisten.
Es reicht sich den Zeilenrang anzuschauen, was gleich 4 ist,
und da dim(V) = 4, erkennt man sofort, dass

    U₁ + U₂ = V.

## Alternativ II für Teilaufgabe 8-1 (4) ##

Man braucht die Aufstellung der Basiselemente und das Gaußverfahren eigentlich nicht. Laut Aufgabenstellung gelten

    U₁ = {x ∈ ℝ⁴ | x₁ + 3·x₂ - 4·x₃ - x₄ = 0} = {a₁}^⊥ = (Lin{a₁})^⊥,
    U₂ = {x ∈ ℝ⁴ | x₁ - 5·x₂ - 2·x₃ - x₄ = 0} = {a₂}^⊥ = (Lin{a₂})^⊥,
    wobei a₁ = (1,3,-4,-1)ᵀ und a₂ = (1,-5,-2,-1)ᵀ

und damit gilt

    U₁ + U₂ = ((U₁ + U₂)^⊥)^⊥
              [hierfür braucht man ein Lemma (1)]
            = (U₁^⊥ ∩ U₂^⊥)^⊥
              [hierfür braucht man ein Lemma (2)]
            = (((Lin{a₁})^⊥)^⊥ ∩ ((Lin{a₂})^⊥)^⊥)^⊥
            = (Lin{a₁} ∩ Lin{a₂})^⊥
              [hier wird Lemma 1 wieder verwendet]
            = ({0})^⊥,
              da {a₁, a₂} offensichtlich lin unabh. sind,
              und damit gibt es kein gemeinsames Element
              in Lin{a₁} ∩ Lin{a₂} außer den Nullvektor
            = V, da alles in V zu 0 senkrecht steht.

Dafür aber brauchen wir einiges über Skalarprodukte zu wissen.
Man braucht folgende Definition und wie angedeutet zwei Lemmata:

**Definition.** Sei V ein Vektorraum mit Skalarprodukt.
Für jede Teilmenge, A ⊆ V, setze man A^⊥ := {x ∈ V | ∀y∈A: ⟨x, y⟩=0}.

**Lemma 1.** Sei V ein endlich dimensionaler Vektorraum mit Skalarpodukt.
Dann (U^⊥)^⊥ = U für alle Untervektorräume, U ⊆ V.
(Für unendlich dimensionale Vektorräume brauchen wir den Begriff eines _abgeschlossenen Untervektorraums_.)

**Lemma 2.** Sei V ein Vektorraum mit Skalarpodukt.
Dann (U₁ + U₂)^⊥ = U₁^⊥ ∩ U₂^⊥ für alle Untervektorräume, U₁, U₂ ⊆ V.
