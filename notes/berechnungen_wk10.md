# Woche 10 #

## §1. Linear oder nicht? ##

In folgenden Aufgaben wird eine Funktion φ : ℝ³ ⟶ ℝ² definiert.
Bestimme in jedem Falle, ob φ linear ist.

a)

    φ(x₁, x₂, x₃) = ( 4·x₁·x₃ )
                    ( 10·x₂   )

Wenn φ linear wäre, dann müsste φ(2, 0, 2) = 2·φ(1, 0, 1) gelten.
Aber:

    φ(2, 0, 2)   = (16, 0)ᵀ
    2·φ(1, 0, 1) = 2·(4, 0)ᵀ = (8, 0)ᵀ ≠  φ(2, 0, 2)

Also ist φ nicht linear, weil Homogenität verletzt wird.

b)

    φ(x₁, x₂, x₃) = ( x₃² )
                    (  0  )

Wenn φ linear wäre, dann müsste φ(0, 0, 8) = 8·φ(0, 0, 1) gelten.
Aber:

    φ(0, 0, 8)   = (64, 0)ᵀ
    8·φ(0, 0, 1) = 8·(1, 0)ᵀ = (8, 0)ᵀ ≠ φ(0, 0, 8)

Also ist φ nicht linear, weil Homogenität verletzt wird.

c)

    φ(x₁, x₂, x₃) = ( x₃ )
                    (  0 )

--> linear

d)

    φ(x₁, x₂, x₃) = ( 0 )
                    ( 0 )

--> linear

e)

    φ(x₁, x₂, x₃) = ( 4  )
                    ( 0  )

Wenn φ linear wäre, dann müsste φ(0) = 0 gelten. [Siehe Lemma 6.1.2]
Aber φ ist hier niemals der Nullvektor!
Also ist φ nicht linear.

f)

    φ(x₁, x₂, x₃) = ( 10·x₃     )
                    (  -x₂ + x₁ )

linear!

g)

    φ(x₁, x₂, x₃) = ( 1 - 10·x₃ )
                    (  -x₂ + x₁ )

Wenn φ linear wäre, dann müsste φ(0) = 0 gelten. [Siehe Lemma 6.1.2]
Aber φ(0) = (1, 0)ᵀ.
Also ist φ nicht linear.

h)

    φ(x₁, x₂, x₃) = ( exp(-(7·x₂ + 8·x₁)) )
                    ( 0                   )

Wenn φ linear wäre, dann müsste φ(0) = 0 gelten. [Siehe Lemma 6.1.2]
Aber φ(0) = (exp(0), 0)ᵀ = (1, 0)ᵀ.
Also ist φ nicht linear.

## §2. Aufgaben ähnlich zu ÜB 10-2 ##

Seien A = (u₁, u₂, u₃) und B = (v₁, v₂),
wobei

    u₁ = (3,  0, 1)ᵀ
    u₂ = (0, -1, 0)ᵀ
    u₃ = (4,  0, 0)ᵀ

    v₁ = (4, 5)ᵀ
    v₂ = (0, 1)ᵀ

Beachte:

- A bildet eine Basis für ℝ³
- B bildet eine Basis für ℝ²

Sei nun φ : ℝ³ ⟶ ℝ² definiert durch

    φ(x₁, x₂, x₃) = ( 4·x₁ - x₃  )
                    ( 10·x₂ + x₁ )

### Zur Linearität ###
Seien

    (x₁,x₂,x₃), (x₁',x₂',x₃') ∈ ℝ³
    c, c' ∈ ℝ

**Zu zeigen:**
    φ(c(x₁, x₂, x₃) +c'(x₁',x₂',x₃')) = c·φ(x₁, x₂, x₃) +c'·φ(x₁', x₂', x₃')

Es gilt

    l. S. = φ(c(x₁, x₂, x₃) +c'(x₁',x₂',x₃'))
          = φ(c(x₁·e1 + x₂·e2 + x₃·e3) +c'(x₁'·e1 + x₂'·e2 + x₃'·e3))
          = φ((c·x₁ + c'·x₁)·e1 + (c·x₂ + c'·x₂)·e2 + (c·x₃ + c'·x₃)·e3)
          = φ(c·x₁ + c'·x₁', c·x₂ + c'·x₂', c·x₃ + c'·x₃')

          = ( 4·(c·x₁ + c'·x₁') - (c·x₃ + c'·x₃')  )
            ( 10·(c·x₂ + c'·x₂') + (c·x₁ + c'·x₁') )

          = ( c·(4·x₁ - x₃)  + c'·(4·x₁' - x₃')  )
            ( c·(10·x₂ + x₁) + c'·(10·x₂' + x₁') )

          = c·( 4·x₁ - x₃  ) + c'·( 4·x₁' - x₃' )
              ( 10·x₂ + x₁ )      ( 10·x₂' + x₁' )

          = r. S.

Darum ist φ linear.

### Darstellung ###
Zunächst beobachten wir:

    φ(x₁, x₂, x₃) = ( 4   0   -1 ) ( x₁ )
                    ( 1   10   0 ) ( x₂ )
                                   ( x₃ )
                  = C·x
                  = φ_C(x)   siehe [Skript, Bsp 6.2.2],

wobei C die Matrix

    C = ( 4   0   -1 )
        ( 1   10   0 )

ist.

**Bemerkung.** Den vorherigen Teil konnten wir hiermit viel einfacher machen:
Da φ_C linear ist (siehe [Skript, Bsp 6.2.2]), ist φ = φ_C linear.

_Zurück zur Berechnung der Darstellung..._

Die zu berechnende Matrix M := M_A^B(φ), ist diejenige, die erfüllt:

- ist x der Form x = ∑ α_j·u_j = (α_j) <--- als Vektor über Basis A
- und ist M·(α_j) = (β_i) für einen Vektor (β_i) ∈ ℝ²
- dann gilt φ(x) = y, wobei y = ∑ β_i·v_i

Zusammengefasst ist M genau die Matrix, für die gilt:

    B·M·α = φ(A·α)

für alle α ∈ ℝ³. Da φ = φ_C, ist die äquivalent zu

    B·M·α = C·A·α

Kurzgesagt: M_A^B(φ) = B^-1 · C · A.
Um dies zu bestimmen, wenden wir das Gaußverfahren
auf folgendes augmentiertes System an

    ( B | C·A )

und reduzieren die linke Hälfte auf die Identitätsmatrix.
Die resultierende Matrix in der rechten Hälfte wir dann M sein.
Es gilt

    C·A = ( 4   0   -1 ) (3   0   4)
          ( 1   10   0 ) (0  -1   0)
                         (1   0   0)
        = ( 11    0   16 )
          (  3  -10    4 )

Also ist das augmentiere System

    ( B | C·A )

     = ( 4    0  |  11    0   16 )
       ( 5    1  |   3  -10    4 )
       Zeile2 <- 4*Zeile2 - 5*Zeile1

    ~> ( 4    0  |   11    0   16 )
       ( 0    4  |  -43  -40  -64 )
       Zeile1 <- Zeile1 : 4
       Zeile2 <- Zeile2 : 4

    ~> ( 1    0  |   11/4    0    4 )
       ( 0    1  |  -43/4  -10  -16 )

Darum gilt

    M_A^B(φ) = (  11/4    0    4 )
               ( -43/4  -10  -16 )



## §3. Lineare Fortsetzung von partiell definierten Funktionen ##

Seien u₁, u₂, u₃, u₄, u₅ eine Basis für ℝ⁵.
Seien v₁, v₂, v₃ Vektoren in ℝ³.
Definiert werden

    φ(u₁) = v₁, φ(u₂) = v₂, φ(u₄) = v₃

**Aufgabe:** Gibt es eine lineare Abbildung, φ : ℝ⁵ ⟶ ℝ³, die die o. s. Gleichungen erfüllen?

**Antwort:** Ja.

**Beweis:**
Da eine (u₁, u₂, u₃, u₄, u₅) Basis für ℝ⁵ ist,
können wir [Skript, Satz 6.1.13] anwenden.
Setze

        φ(u₃) := 0 (Nullvektor)
        φ(u₅) := 0 (Nullvektor)

Mit der partiellen Definition von φ auf der Basis (u₁, u₂, u₃, u₄, u₅),
existiert laut [Skript, Satz 6.1.13] eine **lineare Ausdehnung**
(auch _Fortsetzung_ od. _Erweiterung_ in der Literatur genannt)
φ : ℝ⁵ ⟶ ℝ³, so dass

    φ(u₁) = v₁, φ(u₂) = v₂, φ(u₄) = v₃, φ(u₃) = 0, φ(u₅) = 0.
**QED**

**Bemerkung 1.**
Konkret, da u₁, u₂, u₃, u₄, u₅ eine Basis ist,
existiert für jedes x ∈ ℝ⁵ eindeutige Werte
c₁, c₂, c₃, c₄, c₅ im Körper ℝ,
so dass

    x = ∑ c_i · ui

gilt, und man setzt

    φ(x) := ∑ c_i · φ(u_i).

Mit dieser Definition ist es einfach, die Axiome durchzugehen,
und zu beweisen, dass dies eine lineare Abbildung definiert.

**Bemerkung 2.**
Beachte, dass die _Wahl_ von den φ(u₃), φ(u₅) im o. s. Beispiel beliebig sein kann. Es ist nur entscheidend, dass in der partiellen
Definition wir es mit linear unabhängigen Elementen zu tun haben.
Falls es zu Abhängigkeiten zwischen den Inputvektoren kommt,
müssen wir wie gewohnt auf eine maximale linear unabhängige Teilmenge
reduzieren, und zeigen, dass für die restlichen Inputs,
die Definition kompatibel ist.

Als Beispiel nehmen wir

    u₁ = (1, 0, 1, 0, 0)ᵀ
    u₂ = (1, 2, 1, 0, 0)ᵀ
    u₃ = (0, 1, 0, 0, 0)ᵀ

und φ : ℝ⁵ ⟶ ℝ³ partiell definiert auf {u₁, u₂, u₃}.
Aus (u₁, u₂, u₃) können wir sehen (etwa durch den Gaußalgorithmus),
dass (u₁, u₂) ein maximales linear unabhängiges Teilsystem ist
und

    u₃ = -½u₁ + ½u₂.

Darum können φ(u₁), φ(u₂) beliebig gewählt werden,
umd es muss

    φ(u₃) = -½φ(u₁) + ½φ(u₂)

gelten (entsprechend dem o. s. Verhältnis).

Wenn wir zum Beispiel

    φ(u₁) = ( 4, 2)ᵀ
    φ(u₂) = (-2, 8)ᵀ
    φ(u₃) = (-3, 3)ᵀ

wählen ist, dies erfüllt.
Man kann das l. u. Teilsystem (u₁, u₂)
durch 3 weitere Vektoren zu einer Basis ergänzen
und φ zu einer linearen Abb ausdehnen.

Wenn wir aber

    φ(u₁) = ( 8, 1)ᵀ
    φ(u₂) = (-4, 8)ᵀ
    φ(u₃) = ( 0, 1)ᵀ

wählen ist, ist φ(u₃) = -½φ(u₁) + ½φ(u₂) nicht erfüllt.
Darum lässt sich hier φ **nicht** zu einer linearen Abbildung erweitern.
